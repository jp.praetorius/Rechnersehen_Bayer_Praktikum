\section{Merkmalsextraktion} \label{sec:Merkmale}

Im Anschluss an die Segmentierung erfolgt die Merkmalsextraktion in den ausgeschnitten Regionen eines Bildes. \newline
In dieser Arbeit werden zwei verschiedene Merkmale genutzt. Zum einen wird das sogenannte \ac{HOG} Merkmal verwendet, um Regionen zu extrahieren, die in Form und Struktur dem Ober--/ Unterhitzesymbol ähneln. 
Zum anderen werden sogenannte \textit{Haar-like} Merkmale \parencite{ViolaJones} benutzt, um die verschiedenen Funktionssymbole voneinander zu trennen.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/HOG_example}
	\caption[Darstellung eines \acs{HOG} Merkmals.]{Darstellung des \acs{HOG} Merkmals eines standardisierten Ober--/ Unterhitzesymbols.}
	\label{fig:hogexample}
\end{figure}


\paragraph{\ac{HOG} Merkmale}
Die \ac{HOG} Merkmale setzen sich aus Gradientenrichtungshistogrammen zusammen. Ein Bildausschnitt wird zur Bestimmung des Merkmalsvektors in mehrere Zellen unterteilt. Für jede dieser Zelle werden mit Hilfe eines Sobel-Gradientenfilters mögliche Gradienten und deren Richtungen detektiert. Diese Gradientenrichtungen werden dann in standardmäßig 9 Polarkoordinatenrichtungen in einem Histogramm abgebildet. 
Entscheidend für die Länge eines Merkmalsvektors ist somit die Anzahl der Richtungen, in die ein Histogramm unterteilt wird und die Anzahl der Zellen pro Bildausschnitt, für die ein solches Histogramm bestimmt wird. Nähere Informationen zu \ac{HOG}-Merkmalen können in der von Veröffentlichung \cite{HOG} nachgelesen werden.

Die \ac{HOG} Merkmale wurden zum Training ausschließlich für die Standard-Icons der Ober--/ Unterhitzesymbole berechnet \prettyref{tab:Datensatz}.
Um eine einheitliche Länge der Merkmalsvektoren zu erhalten, werden sowohl die Trainingsbilder, als auch alle Testdaten mit den ausgeschnittenen Regionen der \textit{selective Search}-Methode auf eine Größe von $32\times32$ Pixel skaliert. Eine Größe von jeweils 10 Pixeln pro Zelle erzielte die besten Ergebnisse.
%Bei dem \ac{HOG}-Merkmal geht es darum, das Bild in Zellen aufzuteilen. Ein wichtiger Parameter ist dabei, wie viele Pixel sich in einer Zelle befinden. In dieser Arbeit wurde das \ac{HOG}-Merkmal mit einer Größe von jeweils 4, 8, 10 und 16 ausprobiert. Nach mehrmaligem ausprobieren, stellte sich schnell heraus, dass sich die besten Ergebnisse mit einer Pixelanzahl von 10 Pixel pro Zelle erzielen lassen. Fortan wurde dieser Parameter gewählt. \newline

%Bei der Abbildung durch das \ac{HOG}-Merkmal erzeugten Bildes wurde schnell deutlich, dass eines der Trainings-Standardicons zu stark verrauscht war und somit das Training und nachfolgend unserer Klassifikation signifikant verschlechtert hat. Fortan wurde dieses Trainingsbild aus dem Datensatz gelöscht. 
Um die Anzahl der Trainingsbilder zu erhöhen und auch leichte Perspektivenwechsel bei der Bildaufnahme zu berücksichtigen, wurden alle Trainingsbilder mehrfach in vertikaler Richtung affin transformiert. Dadurch konnten mehr Richtungen der Gradienten erfasst werden und das Training verbessert werden. In Abbildung \ref{fig:hogexample} sind die Gradientenrichtungen für ein transformiertes standardisiertes Ober--/ Unterhitzesymbol zu dargestellt. Insbesondere die rechteckige Struktur des Symbols wird durch das Merkmal gut abgebildet. 
%TODO: Füge Beispiel mit anderem Funktionssymbol hinzu

\paragraph{Haar-like Merkmal}
Zusätzlich zu den \ac{HOG} Merkmalen wurden Klassifikatoren mit \textit{Haar-like} Merkmalen trainiert. Mit Hilfe dieser Merkmal lassen sich ähnliche Strukturen und Regionen innerhalb eines Objektes auf verhältnismäßig einfache und schnelle Art und Weise erkennen. Diese Merkmale wurden ursprünglich in dem Gesichtsdetektor von \citeauthor{ViolaJones} verwendet. 
Bei den \textit{Haar-like} Merkmalen werden die Integral-Bilder verschiedener Bildregionen verglichen. Das Integralbild eines Grauwertbildes mit der Intensitätsverteilung $f$ ist über die Summe
\begin{equation*}
	I(x,y) = \sum_{i=0}^{x} \sum_{j=0}^{y} f(i,j)
\end{equation*}
definiert als die Summe der Intensitätswerte in einem Rechteck oberhalb des Pixels $(x,y)$. Die Differenz solcher Integralbilder kann als Merkmal verwendet werden. Je nach der Anzahl, ergibt sich der Merkmalsvektor.\newline

In der Regel können solche Merkmale automatisch gelernt werden. Da sich jedoch Symbole wie das Ober-/Unterhitze-Symbol und zum Beispiel das Oberhitze-Symbol nur geringfügig unterscheiden, für diese Arbeit jedoch eindeutig unterschieden werden müssen, wurden die Haar-like-Merkmale explizit angegeben.

\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{images/haar_cut}
	\caption[Haar-like-feature]{Zusammensetzung der Haar-like-Feature}
	\label{fig:haarcut}
\end{figure}

Abbildung \ref{fig:haarcut} zeigt eine Auflistung der Zusammensetzung des verwendeten Haar--like-- Merkmalsvektors. Jedes Quadrat beschreibt dabei das ganze Bild. Für die rot und grün gefärbten Flächen wird jeweils das Integral-Bild bestimmt. Die Differenz aus beiden Summen ergibt einen Eintrag im Merkmalsvektor. Der erste Wert ist somit gegeben durch den Ausdruck
\begin{equation*}
	F = \sum_{Pixel\ a \ in \ rot} f(a) - \sum_{Pixel \ b \ in \ gruen} f(b) \ ,
\end{equation*}
die weiteren Merkmale werden analog berechnet.
 
Um das gesuchte Ober-/Unterhitze-Symbol zu beschreiben, lassen sich viele unterschiedliche Zusammensetzungen finden. Nach dem Ausprobieren unterschiedlichster Varianten wurden die besten Ergebnisse mit der Zusammensetzung von Abbildung \ref{fig:haarcut} erzielt. Dabei wurde versucht, die symmetrischen Muster des Ober-/Unterhitze-Symbols bestmöglich abbilden zu können. Dabei muss gleichzeitig die eindeutige Differenzierung von anderen symmetrischen Symbolen, wie zum Beispiel dem \textit{Umluft}-Symbol gewährleistet werden. In Tabelle \ref{tab:Haar} sind die Merkmalsvektoren für verschiedene Funktionssymbole der Größe $20\times20$ Pixel dargestellt.  
%TODO: Korrigiere zu richtiger, finaler Größe

Es wird deutlich, dass das letzte Merkmal, welches ausschließlich aus dem Integralbild der inneren 4 Pixel N \prettyref{fig:haarcut} besteht eine Abgrenzung gegenüber dem Umluftsymbol erzeugt. 
Die Merkmale $\{ \abs{A-C}, \abs{F-I}, \abs{G-H} \} $ grenzen das Ober--/ Unterhitzesymbol u.a. gegen das Ober-- bzw. Unterhitzesymbol ab.  
Mit den Merkmalen $ \{\abs{D-E},\ \abs{F-I},\ \abs{G-H} \}$ wird eine Invarianz gegenüber affin transformierten Symbolen hergestellt. Außerdem berücksichtigen $ \{\abs{J-K},\ \abs{L-M} \}$ zusätzlich den Rand der Funktionssymbole und trennen somit insbesondere von anderen Bildausschnitten der Rückweisungsklasse 2 \prettyref{tab:Datensatz}. \newline
Jeder Merkmalsvektor besteht bei dem Haar-like-Merkmal also aus 9 Einheiten.

\begin{table}[htb]
	\centering
	\caption{Beispielhafte Haar-like-Merkmalsvektoren für verschiedene Funktionssymbole der Größe $ 20\times 20 $ Pixel.}
	\begin{tabular} {c !{\vrule width 2pt} r r r r r}
		\toprule
		& \includegraphics[height=1.5cm]{f1} & \includegraphics[height=1.4cm]{OU_rotate} & \includegraphics[height=1.5cm]{oberhitze} & \includegraphics[height=1.5cm]{grill} & \includegraphics[height=1.5cm]{umluft} \\ 
		\midrule[2pt]
		$\abs{A-B}$ & 7650 	& 5865 	& 7650	& 8925 	& 6885\\
		$\abs{A-C}$ & 0 	& 0		& 6120 	& 6375 	& 1275 \\
		$\abs{B-C}$ & 7650	& 5865 	& 13770 & 15300 & 6885 \\
		$\abs{D-E}$ & 0 	& 0 	& 0 	& 765 	& 255 \\
		$\abs{F-I}$ & 0 	& 0 	& 3060 	& 3570 	& 0 \\
		$\abs{G-H}$ & 0 	& 0 	& 3060 	& 2805 	& 255 \\
		$\abs{J-K}$ & 0 	& 2040 	& 0 	& 0 	& 0 \\
		$\abs{L-M}$ & 0 	& 0	 	& 0 	& 0 	& 510 \\
		$N$ 		& 1020 	& 1020 	& 1020 	& 1020 	& 255\\
		\bottomrule
	\end{tabular}
	\label{tab:Haar}
\end{table}
