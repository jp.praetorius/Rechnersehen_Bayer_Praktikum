\chapter{Ergebnisse}

Die Ergebnisse aus der Anwendung der unterschiedlichen, in Kapitel \ref{sec:Klassifikation}  vorgestellten Klassifikatoren auf die Datensätze aus Tabelle \ref{tab:Datensatz} soll im Folgenden gezeigt werden.

Die Anwendung des gesamten Prozesses von Vorverarbeitung bis zur Klassifikation eines Bild dauert durchschnittlich etwa \SI{15}{\sec}. Diese Zeitangabe hängt jedoch von der Anzahl der \ac{ROI}s im Bild und der Reihenfolge der Klassifikation ab. Wird eine der \ac{ROI}s von der \ac{mSVM} als Ober--/ Unterhitzesymbol klassifiziert, so werden alle anderen \ac{ROI}s nicht weiter untersucht.

\section{Datensatz \enquote{\textit{Eine Perspektive}}}

Zunächst wird das entwickelte System unter der Annahme getestet, dass die Kamera fest installiert ist und die Perspektivenwechsel im Testdatensatz somit eingeschränkt sind. In Abbildung \ref{fig:ROC} sind die \acs{roc}-Kurven (\acl{roc}) drei verschiedener Klassifikatoren für unterschiedliche Schwellwerte auf den Abstand zur \ac{OCC}-Trennfunktion aufgetragen. Die zugehörigen Flächen unterhalb der Kurven (\acf{auc}) sind in Tabelle \ref{tab:AUC} aufgeführt. Die Kombination aus \ac{OCC}-\ac{SVM} und \ac{MLP} weist entsprechend dieser Metrik die beste Performance auf, während die Rate der Falsch-Positiven (\ac{fpr}) für die \ac{OCC} bei wachsendem Schwellwert am stärksten ansteigt. \newline


\begin{wraptable}{r}{0.4\textwidth}
%	\centering
	\caption{\ac{auc} verschiedener Klassifikatoren.} \label{tab:AUC}
	\begin{tabular} {l r}
		\toprule
		Klassifikator & \ac{auc} \\
		\midrule[2pt]
		\ac{OCC}--\ac{SVM} & \SI{89.38}{\percent} \\
		\ac{mSVM} & \SI{91.89}{\percent}\\
		\ac{MLP} & \SI{96.80}{\percent} \\
		\bottomrule
	\end{tabular}
\end{wraptable}


Dieser Schluss legt nahe, dass die \ac{OCC}--\ac{SVM} alleine nicht ausreicht, um die verschiedenen Funktionssymbole voneinander zu trennen. Dieser Schluss wird bestätigt, wenn die fälschlicherweise als Ober--/ Unterhitze-Symbol klassifizierten \ac{ROI}s betrachtet werden. Die auf den \ac{HOG}-Merkmalen trainierte \ac{OCC} kann nicht ausreichend gut zwischen den verschiedenen Funktionssymbolen unterscheiden.
Die Abbildungen \ref{fig:exampleProzess:heatmap_woT} -- \ref{fig:exampleProzess:heatmap_wT} zeigen, dass auch eine reine \ac{mSVM} nicht ausreicht. Ohne eine vorherige \ac{OCC}-Klassifikation würden \ac{ROI}s als Ober--/ Unterhitzesymbol klassifiziert (positiver Abstand), obwohl diese keine sind. Die richtige Zuordnung jedoch gelingt durch die Kombination beider Klassifikatoren.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/ClassifierCompareROC_dark}
	\caption[\ac{roc}-Kurve verschiedener Klassifikatoren.]{\ac{roc}-Kurve verschiedener Klassifikatoren. Aufgetragen ist die \ac{tpr} gegen die \ac{fpr} bei steigendem Schwellwert auf den Abstand zur Trennfunktion der \ac{OCC}.}
	\label{fig:ROC}
\end{figure}


Der Einfluss des gewählten Schwellwerts auf die Genauigkeit der Klassifikatoren wird in Abbildung \ref{fig:classifiercomparebarplot} deutlich. Wie zu erwarten, steigt die Genauigkeit der \ac{OCC} mit wachsendem Schwellwert. Die Ergebnisse für die kombinierten Klassifikatoren unterscheiden sich weniger deutlich. Das beste Ergebnis wird bei der Kombination aus \ac{OCC} und \ac{MLP} mit einer Genauigkeit von \SI{93.2}{\percent} erreicht. 

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/ClassifierCompareBarplot}
	\caption[Genauigkeit der Klassifikatoren bei verschiedenen Schwellwerten t.]{Genauigkeit unterschiedlicher Klassifikatoren bei verschiedenen Schwellwerten für den Abstand zur \ac{OCC}-Trennfunktion.}
	\label{fig:classifiercomparebarplot}
\end{figure}

Relevant für die Wahl des besten Klassifikators sind neben der erreichten Genauigkeit jedoch noch zusätzliche Überlegungen. Im Kontext eines digitalen Vieraugenprinzips kann ein \textit{falsch-positives} Ergebnis, d.h. in dem betrachteten Szenario eine falsch eingestellte Maschine, die nicht als solche erkannt wird gegebenenfalls schwerwiegende Folgen haben. Sollte ein Mitarbeiter sich zu sehr auf die Aussage des Systems verlassen, wird ein solcher Fehler nicht erkannt und die Maschine wird unter verkehrter Einstellung gestartet. Die \ac{fpr} sollte dementsprechend gering gehalten werden. \\
Falsche Negativ--Ergebnisse hingegen bedeuten eine Warnung, obwohl das Gerät richtig eingestellt ist. Ein solches Ereignis kann durch den Mitarbeiter leicht geklärt werden und ist weniger gravierend. Trotzdem sollte die \ac{fnr} so gering wie möglich gehalten werden. Eine Übersicht dieser Raten ist in dem Konfusionsmatrizen in Abbildung \ref{fig:ConfusionMatrix} links für den Schwellwert \num{-0.003} bzw. \num{-0.002} rechts zu sehen.  
Es wird deutlich, dass die \ac{fpr} im Fall der \ac{OCC} deutlich höher ist, als für die kombinierten Klassifikatoren. Die \ac{tpr} ist jedoch entsprechend höher. Gemäß den bisherigen Überlegungen ist dieser Klassifikator jedoch aufgrund der hohen \ac{fpr} nicht geeignet.\\
 Die Wahl zwischen \ac{mSVM} und \ac{MLP} muss individuell und abhängig von der Aufgabe getroffen werden. Die \ac{fpr} ist für die \ac{mSVM} annähernd konstant, sodass dafür der Schwellwert \num{-0.003} mit der höheren \ac{tpr} vorzuziehen ist. Bei dem \ac{MLP} muss zwischen niedrigerer \ac{fpr} bei $ t=-0.002$ und höherer \ac{tpr} $ t=-0.003$ abgewogen werden.



\begin{figure}[!htb]
	
	\begin{minipage}{.45\linewidth}
		\centering
		\subfloat[\ac{OCC}--\ac{SVM}, $ t=-0.003$.] {\label{fig:ConfusionMatrix:OCC}\includegraphics[width=\linewidth]{ConfusionMatrix_OCC}}
	\end{minipage}%
	\begin{minipage}{.45\linewidth}
		\centering
		\subfloat[\ac{OCC}--\ac{SVM}, $ t=-0.002$.] {\label{fig:ConfusionMatrix:OCC}\includegraphics[width=\linewidth]{ConfusionMatrix_OCC_02}}
	\end{minipage}\par 
	\begin{minipage}{.45\linewidth}
		\centering
		\subfloat[Kombination aus \ac{OCC} und \ac{SVM}, $ t=-0.003$.] {\label{fig:ConfusionMatrix:OCCSVM}\includegraphics[width=\linewidth]{ConfusionMatrix_OCCSVM}}
	\end{minipage}%
	\begin{minipage}{.45\linewidth}
		\centering
		\subfloat[Kombination aus \ac{OCC} und \ac{SVM}, $ t=-0.002$.] {\label{fig:ConfusionMatrix:OCCSVM}\includegraphics[width=\linewidth]{ConfusionMatrix_OCCSVM_02}}
	\end{minipage}\par
	\begin{minipage}{.45\linewidth}
		\centering
		\subfloat[Kombination aus \ac{OCC} und \ac{MLP}, $ t=-0.003$.] {\label{fig:ConfusionMatrix:OCCMLP}\includegraphics[width=\linewidth]{ConfusionMatrix_OCCMLP}}
	\end{minipage}%
	\begin{minipage}{.45\linewidth}
		\centering
		\subfloat[Kombination aus \ac{OCC} und \ac{MLP}, $ t=-0.002$.] {\label{fig:ConfusionMatrix:OCCMLP}\includegraphics[width=\linewidth]{ConfusionMatrix_OCCMLP_02}}
	\end{minipage}\par
	\caption{Konfusionsmatrizen für verschiedene Klassifikatoren bei einem Schwellwert auf dem Abstand zur \ac{OCC}--Trennfunktion von \num{-0,003} (links) bzw. \num{-0.002} (rechts).}
	\label{fig:ConfusionMatrix}
\end{figure}

\section{weitere Datensätze}

In Tabelle \ref{tab:Datensatz} sind neben dem Datensatz mit eingeschränkter Perspektive auch ein Test-- und ein Validierungsdatensatz mit variierenden Perspektiven und Abständen beschrieben.
Die Validierungsdaten enthalten ausschließlich Bilder von einem Backofen, der nicht zum Training verwendet wurde.
Die Genauigkeiten der verschiedenen Klassifikatoren für alle drei Datensätze sind in Tabelle \ref{tab:result_accuracy} für den Schwellwert $ t = \num{-0.003}$ zusammengefasst. 


\begin{table}[tb]
	\centering
	\caption{Genauigkeiten der Klassifikatoren bei einem Schwellwert von $ t=\num{-0.003}$ für die verschiedenen Testdatensätze. }
	\begin{tabular} {r r r r}
		\toprule
		& \textbf{\ac{OCC}} & \textbf{\ac{OCC} --- \ac{mSVM}} & \textbf{\ac{OCC} --- \ac{MLP}} \\ 
		\midrule[2pt]
		
		Eine Perspektive & \SI{74.6}{\percent} & \SI{92.4}{\percent} & \SI{93.2}{\percent} \\
		Flexibel & \SI{68.6}{\percent} & \SI{82.5}{\percent} & \SI{76.6}{\percent} \\
		Validierung & \SI{43.5}{\percent} & \SI{82.6}{\percent} & \SI{82.6}{\percent} \\
		\bottomrule
	\end{tabular}
	\label{tab:result_accuracy}
\end{table}

Es wird deutlich, dass insbesondere die Genauigkeit der \ac{OCC}--\ac{SVM} unter dem Perspektivenwechsel sinkt. Dies ist unter anderem dadurch zu erklären, dass das Funktionssymbol bei Backöfen, die aus größerer Distanz aufgenommen wurden, nur noch über wenige Pixel ausgedehnt ist. Die Auflösung ist gering und damit sinkt die Genauigkeit bei der Berechnung des \ac{HOG}--Merkmalsvektors. 
Auch bei einem zu starken Wechsel des Blickwinkels wird das Ober--/ Unterhitzesymbol nicht mehr erkannt, da diese perspektivische Verzerrung in den Trainingsdaten nicht abgedeckt ist. Anderenfalls würden nicht ausgewählte Funktionssymbole, die seitlich auf dem Auswahlknopf des Backofens zu sehen sind, erkannt.\\
Die Annahme der eingeschränkten Perspektive hat entsprechend einen merklichen Einfluss auf die Klassifikationsergebnisse.