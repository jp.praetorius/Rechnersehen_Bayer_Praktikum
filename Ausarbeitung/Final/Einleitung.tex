% Einleitung
\begin{quotation}
	\noindent
	\centering
	\textit{Doppelt hält besser}. \footcite[Vgl.][]{zitate} \\
	\flushright --- Ovid (43 v. Chr. -- 17 n. Chr.)
	
\end{quotation}

Dieses Motto findet sich in den verschiedensten Lebensbereichen wieder. In der Wirtschaft lässt es sich beispielsweise als sogenanntes Vieraugenprinzip zur Kontrolle anwenden. Im Rahmen von Produktionsmaschinen werden die von einer Person vorgenommenen Einstellungen von einem weiteren Mitarbeiter kontrolliert, um Fehler-- und Gefahrenquellen zu minimieren. Um den Personaleinsatz effizienter zu gestalten, ist ein \textit{digitales Vieraugenprinzip} (Digital4Eyes) sinnvoll. Dabei kann der zweite, kontrollierende Mitarbeiter durch ein intelligentes Kamerasystem ersetzt werden, welches die Einstellungen überprüft. Die Entwicklung eines solchen \textit{Digital4Eyes}--Systems, welches verschiedene Aufgaben erfüllen soll, ist Bestandteil des \enquote{Grants4Tech}-Wettbewerbs 2018 des Unternehmens Bayer\footnote{siehe  \url{https://grants4tech.bayer.com/home/}}. \\
Im Rahmen dieses Projekts wurde ein Programm entwickelt, welches in der Lage ist, Kameraaufnahmen von Maschinen auf Ihre Einstellung hin zu untersuchen. 
Anforderungen an ein solches \textit{Digital4Eyes}-System sind insbesondere eine hohe Flexibilität bzgl. der untersuchten Maschine und des gewünschten Zustandes. Dies bedeutet, dass eine Übertragbarkeit auf andere, modifizierte Problemstellungen leicht realisierbar sein soll. Zu diesem Zweck soll die Menge der zu annotierenden Daten gering gehalten werden. \\
Exemplarisch wird in dieser Arbeit die Einstellung von Backöfen untersucht. Ohne Beschränkung der Allgemeinheit wurde die Funktion \textit{Ober--/Unterhitze} als gewünschte, korrekte Einstellung gewählt.

Verglichen wird ein Algorithmus, der nur auf einer Klasse, den \textit{Ober--/Unterhitze}-Symbolen, trainiert wird (\ac{OCC}) mit einem Algorithmus, der zusätzlich andere Symbole berücksichtigt.

\begin{wrapfigure}{r}{0.4\textwidth}
	\includegraphics[width=0.38\textwidth]{f1}
	\caption[Standardisiertes \textit{Ober--/ Unterhitzesymbol}]{Beispiel für ein standardisiertes Ober--/ Unterhitzesymbol.}
	\label{fig:ouStandard}
\end{wrapfigure}
Als Trainingsdaten wurden sowohl standardisierte Backofenfunktionsicons genutzt, wie beispielhaft in Abbildung \ref{fig:ouStandard} zu sehen, als auch Bildausschnitte von Backöfen, die ein Funktionssymbol bzw. andere Regionen zeigen. Die standardisierten Icons werden zusätzlich mittels affinen Transformationen perspektivisch verzerrt. Die Zusammensetzung der beiden Trainingsdatensätze sind in Tabelle \ref{tab:Datensatz} zusammengefasst. \\
Als Testdaten werden Aufnahmen der Größe $ 853 \times 480 $ Pixel von Backöfen aus verschiedenen Perspektiven und Abständen genutzt. Unterschiedenen wird ein Datensatz, der Aufnahmen aus unterschiedlichsten Blickwinkeln und Abständen enthält (\enquote{Test \textit{Flexibel}}), mit einem eingeschränkten Datensatz. Die Bilder in \enquote{Test \textit{Eine Perspektive}} wurden unter geringen Blickwinkel-- und Abstandsänderungen  aufgenommen. Diese Annahme ist in sofern gerechtfertigt, als dass die Kamera bei einem \textit{digitalen 4--Augen}--Aufbau in einem Unternehmen wie Bayer in einer Apparatur fixiert wäre. Die Perspektive würde sich bei den verschiedenen Einsätzen dementsprechend nur geringfügig ändern. \\

\begin{table}[htb]
	\centering
	\caption{Beschreibung der Trainings-- und Testdatensätze.}
	\begin{tabular} {l p{2cm} p{9cm}}
		\toprule
		\textbf{Datensatz} & \multicolumn{2}{c}{\textbf{Beschreibung}} \\ 
		\midrule[2pt]
		Training OCC & \multicolumn{2}{l}{44 standardisierte \textit{Ober--/Unterhitze} Icons.}  \\
		Training Multi-Class & Klasse 0 & 44 standardisierte \textit{Ober--/Unterhitze} Icons \newline + 36 Bildausschnitte  \\
		& Klasse 1 & 130 standardisierte Icons verschiedener Backofen-Funktionen \newline + 129 extrahierte Bildausschnitte verschiedener Backofen-Funktionen aus Backofenaufnahmen\\
		& Klasse 2 & 1335 Bildausschnitte unterschiedlicher Bildregionen, die kein Funktionssymbol zeigen.\\
		\midrule
		Test \textit{Eine Perspektive} & Klasse 0 & 42 Backofenaufnahme mit begrenzter Variation der Perspektive, wobei die \textit{Ober--/ Unterhitze}-Funktion ausgewählt ist. \\
		& Klasse 1 (Rückweisung) & 76 Backofenaufnahme mit begrenzter Variation der Perspektive, wobei die \textit{Ober--/ Unterhitze}-Funktion nicht ausgewählt ist.\\ 
		Test \textit{Flexibel} & Klasse 0 & 46 Backofenaufnahme aus variierenden Blickwinkeln und Abständen, wobei die \textit{Ober--/ Unterhitze}-Funktion ausgewählt ist. \\
		& Klasse 1 (Rückweisung) & 84 Backofenaufnahme aus variierenden Blickwinkeln und Abständen, wobei die \textit{Ober--/ Unterhitze}-Funktion nicht ausgewählt ist. \\
		Validierung & \multicolumn{2}{l}{Aufnahmen eines Backofens, der nicht zum Training verwendet wurde.} \\
		& Klasse 0 & 8 Backofenaufnahmen mit eingeschalteter \textit{Ober--/ Unterhitze}-Funktion.\\
		& Klasse 1 (Rückweisung) & 19 Backofenaufnahmen mit anderen, ausgewählten Backofen-Funktionen.\\
		\bottomrule
	\end{tabular}
	\label{tab:Datensatz}
\end{table}

Unter dieser Annahme wird mit dem in dieser Arbeit eine Genauigkeit von ca. \SI{92}{\percent} auf dem eingeschränkten Testdatensatz erreicht.