import numpy as np
import cv2
import imutils
from matplotlib import pyplot as plt
import selectivesearch
from sklearn import preprocessing
from skimage import data, exposure, transform
from skimage import feature


def whitening(gray):
    return preprocessing.scale(gray)

def equalize_gray_Hist(gray):
    return cv2.equalizeHist(gray)

def equalize_gray_Hist_CLAHE(gray):
    # CLAHE (Contrast Limited Adaptive Histogram Equalization)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(12, 12))
    return clahe.apply(gray)

def adaptive_Threshold(img, bsize=45, offset=30):
    return cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, bsize, offset)

def edgeDetector(gray_eq):
    return imutils.auto_canny(gray_eq)

def hough_transformation_line(img, threshold=70):
    # perform hough transformation to get lines
    # edges: edges from canny/ CLAHE
    # thresh: minimal line length
    # return params: Hough lines in {'rho': rho, 'theta': theta}

    #edges = imutils.auto_canny(img)

    # threshold = round(img.shape[1] * thresh)
    lines = cv2.HoughLines(img, 1, np.pi / 180, threshold)

    if lines is None:
        print('CAUTION: no lines detected')
        lines = -1
        return lines

    params = {}

    for i in range(len(lines)):
        rho = lines[i][0][0]
        theta = lines[i][0][1]

        params[i] = {'rho': rho, 'theta': theta}
    return params

def cutImage(img, hull):
    # cut input image on the range spanned by the convex hull of polygon points
    # hull: convex hull of mask

    mini = np.amin(hull, 0)[0]
    maxi = np.amax(hull, 0)[0]
    return img[mini[1]:maxi[1], mini[0]:maxi[0]]

def extractTrainFeaturesMultiClass(path, size=(14,14)):
    data = []

    # loop over the image paths in the training set
    for subdir, dirs, files in os.walk(path):
        for img in files:
            logo = cv2.imread(subdir+'\\'+img, 0)
            logo = cv2.adaptiveThreshold(logo, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 35, 10)
            feature = getResizedHaarFeature(logo, size)
            data.append(feature)

    return data

def getHOGfeature(gray, ppc=(10, 10), orient=9):
    (H, hogImage) = feature.hog(gray, orientations=orient, pixels_per_cell=ppc,
        cells_per_block=(2, 2), transform_sqrt=True, visualise=True)

    return (H, hogImage)

def getTwoHaarFeature(img, img_hull, img_min_hull):
    (m,n) = img.shape
    (m_hull, n_hull) = img_hull.shape
    (m_min_hull, n_min_hull) = img_min_hull.shape

    # entire image
    P1 = (m-1, n-1)

    # inner of convex convex hull
    P2 = (m_hull-1, n_hull-1)
    P3 = (m_min_hull-1, n_min_hull-1)

    integ = transform.integral_image(img).astype(int)

    A = integ[P1]
    B = integ[P2]
    C = integ[P3]

    feature_vector = np.array([np.abs(A - B), np.abs(A - C)])

    return feature_vector


def getResizedHaarFeature(img):
    (m,n) = img.shape

    P1 = (int(np.floor(2 * m / 5)) - 1, n - 1)
    P2 = (int(np.ceil(3 * m / 5)) - 1, n - 1)
    P3 = (m-1, int(np.floor(n / 2)) - 1)
    P4 = (int(np.floor(m / 2)) - 1, 0)
    P5 = (int(np.floor(m / 2)) - 1, int(np.floor(n / 2)) - 1)
    P6 = (int(np.floor(m / 2)) - 1, n-1)

    integ = transform.integral_image(img).astype(int)

    A = integ[P1]
    B = integ[P2] - integ[P1]
    C = integ[(m - 1, n - 1)] - integ[P2]
    D = integ[P3]
    E = integ[(m-1,n-1)] - integ[P3]
    F = integ[P5]
    G = integ[P6] - integ[P5]
    H = integ[P3] - integ[P5]
    I = integ[(m - 1, n - 1)] - integ[P3] - integ[P6] + integ[P5]
    J = integ[(int((m - 1) / 7), n - 1)]
    K = integ[(int((m - 1)), n - 1)] - integ[(int((m - 1) * 6 / 7), n - 1)]
    L = integ[((m - 1), int((n - 1) / 7))]
    M = integ[(int((m - 1)), n - 1)] - integ[((m - 1), int((n - 1) * 6 / 7))]
    N = integ[int(np.floor((m - 1) / 2)) + 1, int(np.floor((n - 1) / 2)) + 1] + integ[
        int(np.floor((m - 1) / 2)) - 1, int(np.floor((n - 1) / 2)) - 1] \
        - integ[int(np.floor((m - 1) / 2)) + 1, int(np.floor((n - 1) / 2)) - 1] - integ[
            int(np.floor((m - 1) / 2)) - 1, int(np.floor((n - 1) / 2)) + 1]

    featureVec = np.array([np.abs(A - B), np.abs(A - C), np.abs(C - B), np.abs(D - E), np.abs(F - I), np.abs(G - H),
        A, B, C, np.abs(J-K), np.abs(L-M), N])

    return featureVec
