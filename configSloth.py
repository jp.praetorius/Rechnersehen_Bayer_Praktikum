LABELS = (
    {"attributes": {"type":  "polygon",
                    "class": "panel"},
     "item":     "sloth.items.PolygonItem",
     "inserter": "sloth.items.PolygonItemInserter",
     "text":     "panel"
    },

    {"attributes": {"type":  "rect",
                    "class": "button",
                    "id":    ["function", "temperature", "other"]},
     "item":     "sloth.items.RectItem",
     "inserter": "sloth.items.RectItemInserter",
     "text":     "Button"
    },
)