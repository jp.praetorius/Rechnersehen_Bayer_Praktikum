import os
from sklearn.externals import joblib
import warnings
warnings.filterwarnings("ignore")
import sys
sys.path.insert(0, 'scripts')
sys.path.insert(0, 'szenario_status/scripts')
import helper
import prepare_lib
import classify_lib



# LOAD OCC SVM
occ_clf = joblib.load('szenario_status/scripts/models/OCC_clf.pkl')

# # LOAD MULTI CLASS SVM
svm_clf = joblib.load('szenario_status/scripts/models/MultiSVM_clf.pkl')

# LOAD MLP
# mlp_clf = joblib.load('models/MLPclf.pkl')

pn = input("Please enter the file directory: ")
if not os.path.isfile(pn):
    print('File unknown. Please check file directory!')
    sys.exit()

# PREPROCESSING
img = prepare_lib.prepareImage(pn)

gray_CLAHE = img.equalize_gray_Hist_CLAHE()
gray_eq = img.equalize_gray_Hist()

edges_eq = img.edgeDetector(gray_eq)
edges_CLAHE = img.edgeDetector(gray_CLAHE)
edges = edges_eq + edges_CLAHE

img.whitening()

# # Hough
HoughLines = img.hough_transformation_line(edges, 0.15)

# get mask
mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)

# cut image
imgCut = img.cutImage(hull)

# selective search
candidates = img.performSelectiveSearch(scale=2, minRectSize=100, w2h=2, useCutImage=True, sig=0.8, minSize=50)

# classify ROIs
j=0
cPred = 1

for x, y, w, h in candidates:

    window = img.adapThres[y:y + h, x:x + w]

    hog = classify_lib.getResizedHOGfeature(window, ppc=(10,10), size=(32, 32))

    haar = classify_lib.getResizedHaarFeature(window, size=(20, 20))

    pred, dist = classify_lib.predictOneClassSVM(occ_clf, hog)

    # preselect ROIs with OCC threshold
    if dist < -0.03:
        continue

    prediction = svm_clf.predict([haar])

    if prediction[0] == 0:
        cPred = 0
        continue


if cPred==0:
    print('Zustand korrekt!')
else:
    print('Zustand fehlerhaft!')
