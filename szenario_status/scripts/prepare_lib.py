import numpy as np
import numpy.core.multiarray
import cv2
import imutils
from matplotlib import pyplot as plt
import selectivesearch
from sklearn import preprocessing



class prepareImage:

    def __init__(self, imgPath, block_size=35, offset=12):      # block_size=15, offset=8
        self.img = cv2.imread(imgPath, cv2.IMREAD_COLOR)
        self.gray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
        self.adapThres = cv2.adaptiveThreshold(self.gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, block_size, offset)
        self.imgCut = None


    def whitening(self):
        self.gray = preprocessing.scale(self.gray)

    def equalize_gray_Hist(self):
        self.gray = cv2.equalizeHist(self.gray)
        return cv2.equalizeHist(self.gray)

    def equalize_gray_Hist_CLAHE(self):
        # # CLAHE (Contrast Limited Adaptive Histogram Equalization)
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(12, 12))
        return clahe.apply(self.gray)

    def edgeDetector(self, gray_eq):
        return imutils.auto_canny(gray_eq)

    def hough_transformation_line(self, edges, thresh=0.22):
        # perform hough transformation to get lines
        # edges: edges from canny/ CLAHE
        # thresh: minimal line length, relative to image width
        # return params: Hough lines in {'rho': rho, 'theta': theta}

        threshold = round(self.img.shape[1] * thresh)

        lines = cv2.HoughLines(edges, 1, np.pi / 180, threshold)

        if lines is None:
            print('CAUTION: no lines detected')
            lines = -1
            return lines


        params = {}

        for i in range(len(lines)):
            rho = lines[i][0][0]
            theta = lines[i][0][1]

            params[i] = {'rho': rho, 'theta': theta}

        return params

    # use helper functions to cut image on the top and bottom

    def cutImage(self, hull):
    # cut input image on the range spanned by the convex hull of polygon points
    # hull: convex hull of mask

        mini = np.amin(hull, 0)[0]
        maxi = np.amax(hull, 0)[0]

        self.imgCut = self.img[mini[1]:maxi[1], mini[0]:maxi[0]]
        self.adapThres = self.adapThres[mini[1]:maxi[1], mini[0]:maxi[0]]
        self.gray = self.gray[mini[1]:maxi[1], mini[0]:maxi[0]]

        # gray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
        # imgMask = gray * mask
        # imgAdaptThrMask = self.adapThres * mask
        # self.imgCut = self.img[mini[1]:maxi[1], mini[0]:maxi[0]]
        # self.adapThres = imgAdaptThrMask[mini[1]:maxi[1], mini[0]:maxi[0]]
        # self.gray = imgMask[mini[1]:maxi[1], mini[0]:maxi[0]]

    def performSelectiveSearch(self, scale, minRectSize=500, w2h=2., useCutImage=False, sig = 0.8, minSize=20):
        # perform selective search on image
        # scale: parameter for selective search
        # minRectSize: exclude all rectangles smaller than minRectSize
        # w2h: exclude rectangles with ratio w/h > w2h and h/w > w2h:

        if useCutImage:
            img_lbl, regions = selectivesearch.selective_search(self.imgCut, scale=scale, sigma=sig, min_size = minSize)
        else:
            img_lbl, regions = selectivesearch.selective_search(self.img, scale=scale, sigma=sig, min_size = minSize)

        candidates = set()
        for r in regions:
            # excluding same rectangle (with different segments)
            if r['rect'] in candidates:
                continue
            # excluding regions smaller than minRectSize pixels
            if r['size'] < minRectSize:
                continue
            # exclude distorted rects
            x, y, w, h = r['rect']
            if w==0 or h==0 or w / h > w2h or h / w > w2h:
                continue
            candidates.add(r['rect'])

        return candidates
