import numpy as np
import cv2
from matplotlib import pyplot as plt
from matplotlib.colors import BoundaryNorm

class Line(object):

    def __init__(self, data):
            self.first, self.second = data

    def slope(self):
            '''Get the slope of a line segment'''
            (x1, y1), (x2, y2) = self.first, self.second
            try:
                    return (float(y2)-y1)/(float(x2)-x1)
            except ZeroDivisionError:
                    # line is vertical
                    return None

    def yintercept(self, slope):
            '''Get the y intercept of a line segment'''
            if slope != None:
                    x, y = self.first
                    return y - slope * x
            else:
                    return None

    def solve_for_y(self, x, slope, yintercept):
            '''Solve for Y cord using line equation'''
            if slope != None and yintercept != None:
                    return float(slope) * x + float(yintercept)
            else:
                    raise Exception('Can not solve on a vertical line')

    def solve_for_x(self, y, slope, yintercept):
            '''Solve for X cord using line equatio'''
            if slope != 0 and slope:
                    return float((y - float(yintercept))) / float(slope)
            else:
                    raise Exception('Can not solve on a horizontal line')


def calcCoordinates(rho, theta, rows, columns):
    if np.pi/4 < theta < 3/4 * np.pi:
        leftEdge = (0, int(rho / np.sin(theta)))
        rightEdge = (columns, int((rho - columns * np.cos(theta)) / np.sin(theta)))
        return leftEdge, rightEdge
    else:
        topEdge = (int(rho / np.cos(theta)), 0)
        bottomEdge = (int((rho - rows * np.sin(theta)) / np.cos(theta)), rows)
        return topEdge, bottomEdge

def fetchLineCoordinates(M, N, params):
    lines = {}
    lines['vert'] = []
    lines['hori'] = []

    # distinguish for vertical and horizontal edges
    for line, par in params.items():
        theta = par['theta']

        if np.pi / 4 < theta < 3 / 4 * np.pi:
            lines['hori'].append(calcCoordinates(par['rho'], par['theta'], M, N))
        else:
            lines['vert'].append(calcCoordinates(par['rho'], par['theta'], M, N))

    return lines

def extractTopLine(M, N, hLines, thold=0.25):
    minLine = ((0, 0), (N, 0))
    top_thold = int(M * thold)

    # extract top line below the bottom top 32% of the image, format: ((x1,y1),(x2,y2))
    for i, pointPair in enumerate(hLines):
        meanPoint = np.mean([pointPair[0][1], pointPair[1][1]])

        # check if line over 25% of the image
        if meanPoint < top_thold:
            # continue

            if meanPoint > np.amin([minLine[0][1], minLine[1][1]]):
                minLine = pointPair

    return minLine

def extractBottomLine(M, N, hLines, thold=0.65):
    maxLine = ((0, M), (N, M))
    top_thold = int(M * thold)

    # extract top line below the bottom 65% of the image, format: ((x1,y1),(x2,y2))
    for i, pointPair in enumerate(hLines):
        meanPoint = np.mean([pointPair[0][1], pointPair[1][1]])

        # check if line below 65% of the image
        if meanPoint < top_thold:
            continue

        if meanPoint < np.amax([maxLine[0][1], maxLine[1][1]]):
            maxLine = pointPair

    return maxLine

def extractLeftLine(M, N, vLines, thold=0.25):
    maxLine = ((0, 0), (0, M))
    left_thold = int(N * thold)

    # extract left line on 25% of the image, format: ((x1,y1),(x2,y2))
    for i, pointPair in enumerate(vLines):
        meanPoint = np.mean([pointPair[0][0], pointPair[1][0]])

        # check if line below 25% of the image
        if meanPoint > left_thold:
            continue

        if meanPoint > np.amax([maxLine[0][0], maxLine[1][0]]):
            maxLine = pointPair

    return maxLine

def extractRightLine(M, N, vLines, thold=0.65):
    maxLine = ((N, 0), (N, M))
    right_thold = int(N * thold)

    # extract right line on 65% of the image, format: ((x1,y1),(x2,y2))
    for i, pointPair in enumerate(vLines):
        meanPoint = np.mean([pointPair[0][0], pointPair[1][0]])

        # check if line below 65% of the image
        if meanPoint < right_thold:
            continue

        if meanPoint < np.amax([maxLine[0][0], maxLine[1][0]]):
            maxLine = pointPair

    return maxLine

def extractPolygonPoints(t_line, b_line, l_line, r_line, M, N):
    pts = {}
    poly = []

    top_line = Line(t_line)
    bottom_line = Line(b_line)
    left_line = Line(l_line)
    right_line = Line(r_line)

    l_slope = {"top": top_line.slope() if top_line.slope() is not None else np.nan,
               "bottom": bottom_line.slope() if bottom_line.slope() is not None else np.nan,
               "right": right_line.slope() if right_line.slope() is not None else np.nan,
               "left": left_line.slope() if left_line.slope() is not None else np.nan}
    l_yintersept = {"top": top_line.yintercept(l_slope["top"]) if l_slope["top"] is not None else np.nan,
                    "bottom": bottom_line.yintercept(l_slope["bottom"]) if l_slope["bottom"] is not None else np.nan,
                    "right": right_line.yintercept(l_slope["right"]) if l_slope["right"] is not None else np.nan,
                    "left": left_line.yintercept(l_slope["left"]) if l_slope["left"] is not None else np.nan}

    pts["left"] = {}
    pts["right"] = {}
    pts["top"] = {}
    pts["bottom"] = {}

    # first step: calculate points on image borders

    # left line
    # if left line vertical
    if l_line[0][0] == l_line[1][0]:
        pts["left"]["0"] = l_line[0]
        pts["left"]["1"] = l_line[1]
    else:
        if 0 < l_line[0][0] < N:
            pts["left"]["0"] = l_line[0]
        else:
            pts["left"]["0"] = (0, int(l_yintersept["left"]))

        if 0 < l_line[1][0] < N:
            pts["left"]["1"] = l_line[1]
        else:
            pts["left"]["1"] = (0, int(l_yintersept["left"]))

    # right line
    # if right line vertical
    if r_line[0][0] == r_line[1][0]:
        pts["right"]["0"] = r_line[0]
        pts["right"]["1"] = r_line[1]
    else:
        if 0 < r_line[0][0] < N:
            pts["right"]["0"] = r_line[0]
        else:
            pts["right"]["0"] = (N, int(right_line.solve_for_y(N, l_slope["right"], l_yintersept["right"])))

        if 0 < r_line[1][0] < N:
            pts["right"]["1"] = r_line[1]
        else:
            pts["right"]["1"] = (N, int(right_line.solve_for_y(N, l_slope["right"], l_yintersept["right"])))

    # top line
    # if top line horizontal
    if t_line[0][1] == t_line[1][1]:
        pts["top"]["0"] = t_line[0]
        pts["top"]["1"] = t_line[1]
    else:
        if 0 < t_line[0][1] < M:
            pts["top"]["0"] = t_line[0]
        else:
            pts["top"]["0"] = (int(top_line.solve_for_x(0, l_slope["top"], l_yintersept["top"])), 0)

        if 0 < t_line[1][1] < M:
            pts["top"]["1"] = t_line[1]
        else:
            pts["top"]["1"] = (int(top_line.solve_for_x(0, l_slope["top"], l_yintersept["top"])), 0)

    # bottom line
    # if bottom line horizontal
    if b_line[0][1] == b_line[1][1]:
        pts["bottom"]["0"] = b_line[0]
        pts["bottom"]["1"] = b_line[1]
    else:
        if 0 <= b_line[0][1] <= M:
            pts["bottom"]["0"] = b_line[0]
        else:
            pts["bottom"]["0"] = (int(bottom_line.solve_for_x(M, l_slope["bottom"], l_yintersept["bottom"])), M)

        if 0 <= b_line[1][1] <= M:
            pts["bottom"]["1"] = b_line[1]
        else:
            pts["bottom"]["1"] = (int(bottom_line.solve_for_x(M, l_slope["bottom"], l_yintersept["bottom"])), M)


    # # step 2: calculate intersections between neighboring lines (tr, rb, bl, lt)
    intersect_x = {"tr": int(-(l_yintersept["top"] - l_yintersept["right"]) / (l_slope["top"] - l_slope["right"]))
    if l_slope["right"] is not np.nan else r_line[0][0],
                   "rb": int(-(l_yintersept["bottom"] - l_yintersept["right"]) / (l_slope["bottom"] - l_slope["right"]))
                   if l_slope["right"] is not np.nan else r_line[0][0],
                   "bl": int(-(l_yintersept["bottom"] - l_yintersept["left"]) / (l_slope["bottom"] - l_slope["left"]))
                   if l_slope["left"] is not np.nan else l_line[0][0],
                   "lt": int(-(l_yintersept["top"] - l_yintersept["left"]) / (l_slope["top"] - l_slope["left"]))
                   if l_slope["left"] is not np.nan else l_line[0][0]}
    intersect_y = {"tr": int(l_slope["top"] * intersect_x["tr"] + l_yintersept["top"]),
                   "rb": int(l_slope["bottom"] * intersect_x["rb"] + l_yintersept["bottom"]),
                   "bl": int(l_slope["bottom"] * intersect_x["bl"] + l_yintersept["bottom"]),
                   "lt": int(l_slope["top"] * intersect_x["lt"] + l_yintersept["top"])}


    # check if intersection points are within the image
    if (0 <= intersect_x["tr"] <= N) & (0 <= intersect_y["tr"] <= M):
        poly.append((intersect_x["tr"], intersect_y["tr"]))
        del pts["top"]["1"]
        del pts["right"]["0"]
    if (0 <= intersect_x["bl"] <= N) & (0 <= intersect_y["bl"] <= M):
        poly.append((int(intersect_x["bl"]), int(intersect_y["bl"])))
        del pts["bottom"]["0"]
        del pts["left"]["1"]
    if (0 <= intersect_x["rb"] <= N) & (0 <= intersect_y["rb"] <= M):
        poly.append((intersect_x["rb"], intersect_y["rb"]))
        del pts["bottom"]["1"]
        del pts["right"]["1"]
    if (0 <= intersect_x["lt"] <= N) & (0 <= intersect_y["lt"] <= M):
        poly.append((intersect_x["lt"], intersect_y["lt"]))
        del pts["top"]["0"]
        del pts["left"]["0"]

    pts = list(pts.values())

    for i in range(len(pts)):
        if pts[i].values():
            poly.append(list(pts[i].values())[0])

    return poly


def extractMask(M, N, params, tt=.25, tb=.65, tl=0.25, tr=0.65):

    if params == -1:
        top_line = ((0,0), (N,0))
        bot_line = ((0, M), (N, M))

        left_line = ((0, 0), (0, M))
        right_line = ((N, 0), (N, M))
    else:
        lines = fetchLineCoordinates(M, N, params)

        top_line = extractTopLine(M, N, lines['hori'], tt)
        bot_line = extractBottomLine(M, N, lines['hori'], tb)

        left_line = extractLeftLine(M, N, lines['vert'], tl)
        right_line = extractRightLine(M, N, lines['vert'], tr)

    #
    # print(top_line)
    # print(bot_line)
    pts = extractPolygonPoints(top_line, bot_line, left_line, right_line, M, N)

    mask = np.zeros((M, N), np.uint8)

    hull = cv2.convexHull(np.array(pts), clockwise=True)
    cv2.polylines(mask, [hull], True, (255, 255, 255), 3)
    cv2.fillPoly(mask, [hull], (255, 255, 255))

    mask[mask == 0] = 0
    mask[mask == 255] = 1

    return mask, hull
