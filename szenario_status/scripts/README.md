# Digital4Eyes

Digital4Eyes implementation as part of the "Anwendungspraktikum Rechnersehen".
"scenario_status": Python program to classify the state of an oven.
"scenario_line": Python program to check whether a assembly belt is empty.

## Packages needed:
* selectivesearch by AlpacaDB _(https://github.com/AlpacaDB/selectivesearch)_
* imutils
* sklearn
* cv2

## Demo
Demo version for scenario_status. Usage:
__python szenario_status\scripts\demo.py__

