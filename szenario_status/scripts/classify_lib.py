import numpy as np
import os
import cv2
import imutils
from skimage import feature
from skimage import data, exposure, transform
from sklearn import svm
from sklearn.externals import joblib
from matplotlib import pyplot as plt


def getHOGfeature(gray, ppc=(10, 10), showHist=False,orient=9):
    # compute HOG feature vector for gray image
    # gray: gray-scaled image
    # ppc: pixel per cell for HOG-features
    # showHist: boolean, show visualization of HOG feature
    # orient: orientations of HOG features


    # edged = imutils.auto_canny(gray)
    (H, hogImage) = feature.hog(gray, orientations=orient, pixels_per_cell=ppc,
        cells_per_block=(2, 2), transform_sqrt=True, visualise=True)

    if showHist:
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4), sharex=True, sharey=True)

        ax1.axis('off')
        ax1.imshow(gray, cmap=plt.cm.gray)
        ax1.set_title('Input image')
        ax1.set_adjustable('box-forced')

        # Rescale histogram for better display
        hogImage_rescaled = exposure.rescale_intensity(hogImage, in_range=(0, 10))

        ax2.axis('off')
        ax2.imshow(hogImage_rescaled, cmap=plt.cm.gray)
        ax2.set_title('Histogram of Oriented Gradients')
        ax1.set_adjustable('box-forced')
        plt.show()
    return H

def getResizedHOGfeature(image, size=(32, 32), ppc=(10,10), showHist=False):
    img = cv2.resize(image, size)
    return getHOGfeature(img, ppc, showHist = showHist)

def getResizedHaarFeature(image, size=(20,20)):
    # get haar-like feature vector for predefined image size
    # image: image to extract feature vector
    # size: size to rescale images, size = (height, width)


    (m,n) = size
    img = cv2.resize(image, (n, m))

    P1 = (int(np.floor(2 * m / 5)) - 1, n - 1)
    P2 = (int(np.ceil(3 * m / 5)) - 1, n - 1)
    P3 = (m-1, int(np.floor(n / 2)) - 1)
    P4 = (int(np.floor(m / 2)) - 1, 0)
    P5 = (int(np.floor(m / 2)) - 1, int(np.floor(n / 2)) - 1)
    P6 = (int(np.floor(m / 2)) - 1, n-1)


    integ = transform.integral_image(img).astype(int)
    A = integ[P1]
    B = integ[P2] - integ[P1]
    C = integ[(m - 1, n - 1)] - integ[P2]
    D = integ[P3]
    E = integ[(m-1,n-1)] - integ[P3]
    F = integ[P5]
    G = integ[P6] - integ[P5]
    H = integ[P3] - integ[P5]
    I = integ[(m - 1, n - 1)] - integ[P3] - integ[P6] + integ[P5]
    J = integ[(int((m - 1) / 7), n - 1)]
    K = integ[(int((m - 1)), n - 1)] - integ[(int((m - 1) * 6 / 7), n - 1)]
    L = integ[((m - 1), int((n - 1) / 7))]
    M = integ[(int((m - 1)), n - 1)] - integ[((m - 1), int((n - 1) * 6 / 7))]
    N = integ[int(np.floor((m - 1) / 2)) + 1, int(np.floor((n - 1) / 2)) + 1] + integ[
        int(np.floor((m - 1) / 2)) - 1, int(np.floor((n - 1) / 2)) - 1] \
        - integ[int(np.floor((m - 1) / 2)) + 1, int(np.floor((n - 1) / 2)) - 1] - integ[
            int(np.floor((m - 1) / 2)) - 1, int(np.floor((n - 1) / 2)) + 1]


    featureVec = np.array([np.abs(A - B), np.abs(A - C), np.abs(C - B), np.abs(D - E), np.abs(F - I), np.abs(G - H),
                           A, B, C, np.abs(J-K), np.abs(L-M), N])
    return featureVec


def extractTrainFeaturesMultiClass(path, size=(20,20), adaptTH = True):
    # extract haar-like features for training data by path
    # path: directory of training images
    # size: size of images to be rescaled for feature extraction size = (height, width)
    # adaptTH: boolean, True: use images with adaptive threshold, False: use binary threshold


    data = []

    # loop over the image paths in the training set
    for subdir, dirs, files in os.walk(path):
        for img in files:
            logo = cv2.imread(subdir+'\\'+img, 0)
            logo = cv2.resize(logo, size)
            if adaptTH:
                logo = cv2.adaptiveThreshold(logo, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 15, 8)
            else:
                _, logo = cv2.threshold(logo, 220, 255, cv2.THRESH_BINARY)
            feature = getResizedHaarFeature(logo, size)
            data.append(feature)
            # print(data)

    return data


def extractTrainFeatures(path, imgSize = (32,32), ppc = (10,10)):
    # extract HOG features for training data by path
    # path: directory of training images
    # imgSize: size of images to be rescaled for feature extraction size = (height, width)
    # ppc: pixel per cell for HOG-features

    data = []

    # loop over the image paths in the training set
    for subdir, dirs, files in os.walk(path):
        for img in files:
            logo = cv2.imread(subdir+'\\'+img, 0)
            # logo = cv2.adaptiveThreshold(logo, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 35, 10)
            _, logo = cv2.threshold(logo, 220, 255, cv2.THRESH_BINARY)
            hog_feature = getResizedHOGfeature(logo, size = imgSize, ppc = ppc)
            data.append(hog_feature)

    return data

def trainOneClassSVM(TrainPath, OutputPath, nu=.5, gamma='auto', imgSize = (32,32), ppc = (10,10)):
    # train the OCC SVM with HOG features
    # TrainPath: path to training data
    # OutputPath: path to save classifier
    # nu: nu parameter of sklearn svm.OneClassSVM
    # gamma: gamma parameter of sklearn svm.OneClassSVM
    # imgSize: size of images to be rescaled for feature extraction size = (height, width)
    # ppc: pixel per cell for HOG-features

    data = extractTrainFeatures(TrainPath, imgSize = imgSize, ppc=ppc)
    # print(data)
    clf = svm.OneClassSVM(kernel="rbf", tol=0.001, nu=nu, gamma=gamma)
    clf.fit(data)
    if OutputPath:
        joblib.dump(clf, OutputPath)
    return clf

def predictOneClassSVM(clf, x_pred_hog):
    # get prediction and distance to decision function of OCC
    # clf: trained OCC classifier
    # x_pred_hog: prediction of OCC

    y_predict = clf.predict(x_pred_hog.reshape(1, -1))
    return y_predict, clf.decision_function(x_pred_hog.reshape(1, -1))
