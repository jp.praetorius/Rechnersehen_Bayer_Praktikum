from __future__ import (
    division,
    print_function,
)

import skimage.data
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import scipy.signal
import selectivesearch
import cv2
import imutils
import os
from os import listdir
import glob
from sklearn.externals import joblib
from timeit import default_timer as timer

import helper
from prepare_lib import *
from classify_lib import *


clf = joblib.load('models/classifier.pkl')
print('Model: ', clf)

pn = '../images/train/10/20171118_133018.jpg'
# pn = '../images/test/10/DSC_0043.JPG'
# pn = 'HOG/test/20171118_130322.jpg'

img = prepareImage(pn)

###### Preprocessing

gray_eq = img.equalize_gray_Hist()
gray_CLAHE = img.equalize_gray_Hist_CLAHE()

# edges_eq = img.edgeDetector(gray_eq)
# edges_CLAHE = img.edgeDetector(gray_CLAHE)
# edges = edges_eq + edges_CLAHE
edges = img.edgeDetector(img.adapThres)

# # Hough
HoughLines = img.hough_transformation_line(edges, 0.22)

# get mask
mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)

# cut image
img.cutImage(hull)

# get window candidates
candidates = img.performSelectiveSearch(450, minRectSize=2000, w2h=1.4, useCutImage=True)
candidates = helper.reduceRect(candidates)

# perform classification on image segments
for x, y, w, h in candidates:
    # print(x, y, w, h)
    start = timer()
    section = img.adapThres[y:y+h, x:x+w]
    M, N = section.shape

    winx = winy = np.min([np.uint8(np.floor(N/8)), np.uint8(np.floor(M /8))])
    print('window size: ', winx, winy)

    stepsize = 10
    heatmaps, yclass = classifyAndCreateHeatmap(section, clf, stepSize=stepsize, winSize=(64, 64), showSlidingWindow=False)

    bestlevel, gmax = getBestLevel(heatmaps, 3)
    print('Globale Maxima pro Level', gmax)
    top3 = helper.getTopNvalues(heatmaps, bestlevel, 3)
    h0 = heatmaps[bestlevel]
    cmap, norm = helper.defineCenteredCmap('bwr', -0.2, gmax[bestlevel])

    fig, ax = plt.subplots(ncols=2, nrows=1)
    ax[0].imshow(section)

    cax = ax[1].imshow(h0, norm=norm, cmap=cmap, vmin=-0.2, vmax=np.max(top3))
    cbar = fig.colorbar(cax, orientation='vertical')

    for val in top3:
        ind = np.argwhere(h0 == val)
        (y, x, h, w) = cv2.boundingRect(ind)

        rect = mpatches.Rectangle((x, y), w, h, fill=False, edgecolor='green', linewidth=2)
        ax[1].add_patch(rect)

    duration = timer() - start
    print(duration)
    plt.show()
