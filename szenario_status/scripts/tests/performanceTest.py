from timeit import default_timer as timer
import cv2
import os
from os import listdir
import matplotlib.pyplot as plt
import importlib
import scipy.misc
import selectivesearch
import matplotlib.patches as mpatches
import numpy as np
from sklearn import svm
from sklearn.metrics import roc_curve, auc
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
import sklearn
import time
from sklearn.externals import joblib
import csv
import pandas as pd

import sys
sys.path.insert(0, 'scripts')
import helper
import prepare_lib
import classify_lib
import time

ppc = (10,10)
# TRAIN OCC SVM
path = '../images/train_icons/o_u_standardIcon/'
# clfOCC = classify_lib.trainOneClassSVM(TrainPath = path, OutputPath='', imgSize = (32,32), ppc = ppc, gamma = 'auto')
# joblib.dump(clfOCC, 'models/OCC_clf.pkl')
clfOCC = joblib.load('models/OCC_clf.pkl')

size=20
# Compute feautures for  multi-class classifier
# class 0
pathSI = '../images/train_icons/o_u_standardIcon/'
pathExt = '../images/train_icons/o_u_symbol/'
# X_o_u_heat = np.array(classify_lib.extractTrainFeaturesMultiClass(path=pathSI, size=(size, size), adaptTH=False))
# X_o_u_heat = np.append(X_o_u_heat, np.array(
#     classify_lib.extractTrainFeaturesMultiClass(path=pathExt, size=(size, size), adaptTH=True)), axis=0)
X_o_u_heat = np.array(classify_lib.extractTrainFeaturesMultiClass(path=pathSI, size=(size, size)))
X_o_u_heat = np.append(X_o_u_heat, np.array(
    classify_lib.extractTrainFeaturesMultiClass(path=pathExt, size=(size, size))), axis=0)


# class 1
pathSI = '../images/train_icons/anti_train/standardIcon/'
X_anti = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(size, size)))
pathExt = '../images/train_icons/anti_train/extracted/'
# X_anti = np.append(X_anti,
#                    np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(size, size), adaptTH=True)),
#                    axis=0)
X_anti = np.append(X_anti,
                   np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(size, size))),
                   axis=0)

# class 2
path_reject = '../images/train_icons/anti_rejection'
# X_anti_reject = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path_reject, size=(size, size), adaptTH=True))
X_anti_reject = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path_reject, size=(size, size)))

# print(X_o_u_heat.shape, X_anti.shape, X_anti_reject.shape)


X = np.concatenate((np.array(X_o_u_heat), np.array(X_anti), np.array(X_anti_reject)))
y = np.concatenate((np.zeros(len(X_o_u_heat)), np.ones(len(X_anti)), 2 * np.ones(len(X_anti_reject))))

# # TRAIN MULTI CLASS SVM
clfSVM = svm.SVC(decision_function_shape='ovr', C=10, tol=0.001, gamma=0.00000001)
clfSVM.fit(X, y)
# joblib.dump(clfSVM, 'models/MultiSVM_clf.pkl')
# clfSVM = joblib.load('models/MultiSVM_clf.pkl')

#  # Train MLP
scaler = StandardScaler()
scaler.fit(X)
Xscale = scaler.transform(X)
clfMLP = MLPClassifier(solver='lbfgs', alpha=10, hidden_layer_sizes=(29, 29, 29), random_state=1, warm_start=True)
# clfMLP = MLPClassifier(solver='lbfgs', alpha=10, hidden_layer_sizes=(50, 50, 50), random_state=1, warm_start=True)
clfMLP.fit(Xscale, y)


cwd = os.getcwd()
# print(cwd)
basepath00 = os.path.join(cwd, '..\\images\\test_onePerspektive\\00')
basepath01 = os.path.join(cwd, '..\\images\\test_onePerspektive\\01')
basepath10 = os.path.join(cwd, '..\\images\\test_onePerspektive\\10')
basepath11 = os.path.join(cwd, '..\\images\\test_onePerspektive\\11')
basepathList = [basepath11, basepath10] #, basepath01, basepath00]
outPath = os.path.join(cwd, '..\\images\\train_icons\\anti')

# yLabelTrue = np.concatenate([np.zeros(len(os.listdir(basepath11))), np.zeros(len(os.listdir(basepath10))),
#                              np.ones(len(os.listdir(basepath01))), np.ones(len(os.listdir(basepath00)))])
# print(yLabelTrue)

t = -0.003
i=0
# timelist = []

for basepath in basepathList:
    timelist = []

    print ('*****************************')
    print(basepath)
    print(os.listdir(basepath))
    print ('*****************************')
    # filelist = []
    for file in os.listdir(basepath):

        pn = os.path.join(basepath, file)
        # print('PN:', pn)
        # filelist.append(pn)
        # print(filelist)
        start = time.process_time()

        # PREPROCESSING
        img = prepare_lib.prepareImage(pn)

        # Preprocessing
        gray_CLAHE = img.equalize_gray_Hist_CLAHE()
        gray_eq = img.equalize_gray_Hist()

        edges_eq = img.edgeDetector(gray_eq)
        edges_CLAHE = img.edgeDetector(gray_CLAHE)
        edges = edges_eq + edges_CLAHE

        img.whitening()

        # # Hough
        HoughLines = img.hough_transformation_line(edges, 0.15)

        # get mask
        mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)

        # perform GrabCut
        # imgGC = img.performGrabCut(mask, 6)

        # cut image
        imgCut = img.cutImage(hull)

        # selective search
        # s=int(scale)
        candidates = img.performSelectiveSearch(scale=2, minRectSize=100, w2h=2, useCutImage=True, sig=0.8, minSize=50)
        # print(len(candidates))

        j = 0
        cPred = 1

        for x, y, w, h in candidates:

            window = img.adapThres[y:y + h, x:x + w]
            winColor = img.imgCut[y:y + h, x:x + w]

            hog = classify_lib.getResizedHOGfeature(window, ppc=ppc, size=(32, 32))

            haar = classify_lib.getResizedHaarFeature(window)
            # haar = scaler.transform([haar])

            pred, dist = classify_lib.predictOneClassSVM(clfOCC, hog)
            #     print(dist)

            if dist < t:  # -0.02:  # pred == 1:#dist > -0.05:
                continue

            # TODO: add threshold on value (HSV) for classification !!!!!
            hsv = cv2.cvtColor(winColor, cv2.COLOR_BGR2HSV)
            if np.mean(hsv[:][2]) > 200:
                print('HSV Criterion:', np.mean(hsv[:][2]))
                plt.imshow(winColor)
                plt.show(block=False)
                plt.pause(3)
                plt.close()
                continue

            # print('************', haar.shape)
            # print(haar)
            # print('************')
            # print('OCC Distance:', dist)
            prediction = clfSVM.predict([haar])
            # distCLF = clf.decision_function([haar])

            # print('occ-distance: ', dist)
            # print('prediction: ', prediction, distCLF)
            if prediction[0] == 0:
                cPred = 0
                # timelist.append(time.process_time() - start)
                continue

            # cPred = np.min([cPred, PredOCC])
        timelist.append(time.process_time() - start)

            # predictionOCC_SVM = clfSVM.predict([haar])
            # predictionOCC_MLP = clfMLP.predict(haarScale)
            #
            # if cPredOCC == -1:
            #     cPredOCC = 0
            #
            # if predictionOCC_SVM[0] == 0:
            #     cPredOCC_SVM = 0
            #     # continue
            # if predictionOCC_MLP[0] == 0:
            #     cPredOCC_MLP = 0
            #
            # # if every classifier predicts class 0, stop looking for further candidates
            # if cPredOCC == 0 & cPredOCC_SVM == 0 & cPredOCC_MLP == 0: # & cPredSVM == 0 & cPredMLP == 0:
            #     # print(cPredSVM, cPredMLP)
            #     continue

    print('****  Modus  *****')
    print(timelist)
    print('durchschnittliche Zeit:')
    print(sum(timelist) / float(len(timelist)))
    i = i+1
