from timeit import default_timer as timer
import cv2
import os
from os import listdir
import matplotlib.pyplot as plt
import importlib
import scipy.misc
import selectivesearch
import matplotlib.patches as mpatches
import numpy as np
from sklearn import svm
from sklearn.metrics import roc_curve, auc
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
import sklearn
import time
from sklearn.externals import joblib
import csv
import pandas as pd

import sys
sys.path.insert(0, 'scripts')
import helper
import prepare_lib
import classify_lib

ppc = (10,10)
# TRAIN OCC SVM
path = '../images/train_icons/o_u_standardIcon/'
clfOCC = classify_lib.trainOneClassSVM(TrainPath = path, OutputPath='', imgSize = (32,32), ppc = ppc, gamma = 'auto')
joblib.dump(clfOCC, 'models/OCC_clf.pkl')
# clfOCC = joblib.load('models/OCC_clf.pkl')

size=20
# Compute feautures for  multi-class classifier
# class 0
pathSI = '../images/train_icons/o_u_standardIcon/'
pathExt = '../images/train_icons/o_u_symbol/'
X_o_u_heat = np.array(classify_lib.extractTrainFeaturesMultiClass(path=pathSI, size=(size, size), adaptTH=False))
X_o_u_heat = np.append(X_o_u_heat, np.array(
    classify_lib.extractTrainFeaturesMultiClass(path=pathExt, size=(size, size), adaptTH=True)), axis=0)
# X_o_u_heat = np.array(classify_lib.extractTrainFeaturesMultiClass(path=pathSI, size=(size, size)))
# X_o_u_heat = np.append(X_o_u_heat, np.array(
#     classify_lib.extractTrainFeaturesMultiClass(path=pathExt, size=(size, size))), axis=0)


# class 1
pathSI = '../images/train_icons/anti_train/standardIcon/'
X_anti = np.array(classify_lib.extractTrainFeaturesMultiClass(path=pathSI, size=(size, size)))
pathExt = '../images/train_icons/anti_train/extracted/'
X_anti = np.append(X_anti,
                   np.array(classify_lib.extractTrainFeaturesMultiClass(path=pathExt, size=(size, size), adaptTH=True)),
                   axis=0)
# X_anti = np.append(X_anti,
#                    np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(size, size))),
#                    axis=0)

# class 2
path_reject = '../images/train_icons/anti_rejection'
X_anti_reject = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path_reject, size=(size, size), adaptTH=True))
# X_anti_reject = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path_reject, size=(size, size)))

# print(X_o_u_heat.shape, X_anti.shape, X_anti_reject.shape)


X = np.concatenate((np.array(X_o_u_heat), np.array(X_anti), np.array(X_anti_reject)))
y = np.concatenate((np.zeros(len(X_o_u_heat)), np.ones(len(X_anti)), 2 * np.ones(len(X_anti_reject))))

# # TRAIN MULTI CLASS SVM
clfSVM = svm.SVC(decision_function_shape='ovr', C=10, tol=0.001, gamma=0.00000001)
clfSVM.fit(X, y)
joblib.dump(clfSVM, 'models/MultiSVM_clf.pkl')
# clfSVM = joblib.load('models/MultiSVM_clf.pkl')

#  # Train MLP
scaler = StandardScaler()
scaler.fit(X)
Xscale = scaler.transform(X)
clfMLP = MLPClassifier(solver='lbfgs', alpha=10, hidden_layer_sizes=(29, 29, 29), random_state=1, warm_start=True)
# clfMLP = MLPClassifier(solver='lbfgs', alpha=10, hidden_layer_sizes=(50, 50, 50), random_state=1, warm_start=True)
clfMLP.fit(Xscale, y)
joblib.dump(clfMLP, 'models/MLPclf.pkl')

cwd = os.getcwd()
# print(cwd)
basepath00 = os.path.join(cwd, '..\\images\\test_onePerspektive\\00')
basepath01 = os.path.join(cwd, '..\\images\\test_onePerspektive\\01')
basepath10 = os.path.join(cwd, '..\\images\\test_onePerspektive\\10')
basepath11 = os.path.join(cwd, '..\\images\\test_onePerspektive\\11')
# basepath00 = os.path.join(cwd, '..\\images\\validation\\00')
# basepath01 = os.path.join(cwd, '..\\images\\validation\\01')
# basepath10 = os.path.join(cwd, '..\\images\\validation\\10')
# basepath11 = os.path.join(cwd, '..\\images\\validation\\11')
basepathList = [basepath11, basepath10, basepath01, basepath00]
outPath = os.path.join(cwd, '..\\images\\train_icons\\anti')

yLabelTrue = np.concatenate([np.zeros(len(os.listdir(basepath11))), np.zeros(len(os.listdir(basepath10))),
                             np.ones(len(os.listdir(basepath01))), np.ones(len(os.listdir(basepath00)))])
print(yLabelTrue)


cnames = ['Th_-005_OCC', 'Th_-005_SVM', 'Th_-005_MLP', 'Th_-005_OCC+SVM', 'Th_-005_OCC+MLP']

# cnames = ['Th_-006_OCC', 'Th_-006_SVM', 'Th_-006_MLP', 'Th_-006_OCC+SVM', 'Th_-006_OCC+MLP',
#           'Th_-005_OCC', 'Th_-005_SVM', 'Th_-005_MLP', 'Th_-005_OCC+SVM', 'Th_-005_OCC+MLP',
#           'Th_-004_OCC', 'Th_-004_SVM', 'Th_-004_MLP', 'Th_-004_OCC+SVM', 'Th_-004_OCC+MLP',
#           'Th_-003_OCC', 'Th_-003_SVM', 'Th_-003_MLP', 'Th_-003_OCC+SVM', 'Th_-003_OCC+MLP',
#           'Th_-002_OCC', 'Th_-002_SVM', 'Th_-002_MLP', 'Th_-002_OCC+SVM', 'Th_-002_OCC+MLP']

# cnames = ['Th_-005_Size_20_OCC', 'Th_-005_Size_20_SVM', 'Th_-005_Size_20_MLP', 'Th_-005_Size_20_OCC+SVM', 'Th_-005_Size_20_OCC+MLP',
#           'Th_-005_Size_22_OCC', 'Th_-005_Size_22_SVM', 'Th_-005_Size_22_MLP', 'Th_-005_Size_22_OCC+SVM', 'Th_-005_Size_22_OCC+MLP',
#           'Th_-004_Size_20_OCC', 'Th_-004_Size_20_SVM', 'Th_-004_Size_20_MLP', 'Th_-004_Size_20_OCC+SVM', 'Th_-004_Size_20_OCC+MLP',
#           'Th_-004_Size_22_OCC', 'Th_-004_Size_22_SVM', 'Th_-004_Size_22_MLP', 'Th_-004_Size_22_OCC+SVM', 'Th_-004_Size_22_OCC+MLP',
#           'Th_-003_Size_20_OCC', 'Th_-003_Size_20_SVM', 'Th_-003_Size_20_MLP', 'Th_-003_Size_20_OCC+SVM', 'Th_-003_Size_20_OCC+MLP',
#           'Th_-003_Size_22_OCC', 'Th_-003_Size_22_SVM', 'Th_-003_Size_22_MLP', 'Th_-003_Size_22_OCC+SVM', 'Th_-003_Size_22_OCC+MLP']

output = pd.DataFrame(data=np.zeros([3, 5]), columns=cnames)
outputLabels = pd.DataFrame(columns=['yTrue', 'OCC', 'SVM', 'MLP', 'OCC+SVM', 'OCC+MLP'])

# i=0
fpr = dict()
tpr = dict()
roc_auc = dict()
# yLabelPredict = []
# yLabelPredictOCC = []
# yLabelPredictMLP = []
# for t in [ -0.06, -0.05, -0.04, -0.03, -0.02]: # np.linspace(15, 16, 2):
# for t, size, name in zip([ -0.05, -0.04, -0.03, -0.05, -0.04, -0.03, -0.05, -0.04, -0.03],
#                          [20, 20, 20, 22, 22, 22],
#                          ['Th_-005_Size_20', 'Th_-004_Size_20', 'Th_-003_Size_20',
#                           'Th_-005_Size_22', 'Th_-004_Size_22', 'Th_-003_Size_22']):
for t, name in zip([-0.03], ['Th_-005']):

    # ppc = (10, 10)
    # TRAIN OCC SVM
    # path = '../images/train_icons/o_u_standardIcon/'
    # clfOCC = classify_lib.trainOneClassSVM(TrainPath=path, OutputPath='', imgSize=(32, 32), ppc=ppc, gamma='auto')
    # joblib.dump(clfOCC, 'models/OCC_clf.pkl')
    # clfOCC = joblib.load('models/OCC_clf.pkl')


    # # Compute feautures for  multi-class classifier
    # # class 0
    # pathSI = '../images/train_icons/o_u_standardIcon/'
    # pathExt = '../images/train_icons/o_u_symbol/'
    # X_o_u_heat = np.array(classify_lib.extractTrainFeaturesMultiClass(path=pathSI, size=(size, size), adaptTH=False))
    # X_o_u_heat = np.append(X_o_u_heat, np.array(
    #     classify_lib.extractTrainFeaturesMultiClass(path=pathExt, size=(size, size), adaptTH=True)), axis=0)
    #
    # # class 1
    # pathSI = '../images/train_icons/anti_train/standardIcon/'
    # X_anti = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(size, size), adaptTH=False))
    # pathExt = '../images/train_icons/anti_train/extracted/'
    # X_anti = np.append(X_anti,
    #                    np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(size, size), adaptTH=True)),
    #                    axis=0)
    #
    # # class 2
    # path_reject = '../images/train_icons/anti_rejection'
    # X_anti_reject = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path_reject, size=(size, size), adaptTH=True))
    #
    # # print(X_o_u_heat.shape, X_anti.shape, X_anti_reject.shape)
    #
    #
    # X = np.concatenate((np.array(X_o_u_heat), np.array(X_anti), np.array(X_anti_reject)))
    # y = np.concatenate((np.zeros(len(X_o_u_heat)), np.ones(len(X_anti)), 2 * np.ones(len(X_anti_reject))))
    #
    # # # TRAIN MULTI CLASS SVM
    # clfSVM = svm.SVC(decision_function_shape='ovr', C=10, tol=0.001, gamma=0.00000001)
    # clfSVM.fit(X, y)
    # # joblib.dump(clfSVM, 'models/MultiSVM_clf.pkl')
    # # clfSVM = joblib.load('models/MultiSVM_clf.pkl')
    #
    # #  # Train MLP
    # scaler = StandardScaler()
    # scaler.fit(X)
    # Xscale = scaler.transform(X)
    # # clfMLP = MLPClassifier(solver='lbfgs', alpha=10, hidden_layer_sizes=(29, 29, 29), random_state=1, warm_start=True)
    # clfMLP = MLPClassifier(solver='lbfgs', alpha=10, hidden_layer_sizes=(50, 50, 50), random_state=1, warm_start=True)
    # clfMLP.fit(Xscale, y)





    i=0
    print('**********************************   OCC Threshold, Size  **********************************')
    # size = (int(s),int(s))
    print(t, size)
    yLabelPredictOCC_SVM = []
    yLabelPredictSVM = []
    yLabelPredictOCC = []
    yLabelPredictOCC_MLP = []
    yLabelPredictMLP = []

    for basepath in basepathList:

        print ('*****************************')
        print(basepath)
        print(os.listdir(basepath))
        print ('*****************************')
        # filelist = []
        for file in os.listdir(basepath):

            pn = os.path.join(basepath, file)
            # print('PN:', pn)
            # filelist.append(pn)
            # print(filelist)

            # PREPROCESSING
            img = prepare_lib.prepareImage(pn)

            # Preprocessing
            gray_CLAHE = img.equalize_gray_Hist_CLAHE()
            gray_eq = img.equalize_gray_Hist()

            edges_eq = img.edgeDetector(gray_eq)
            edges_CLAHE = img.edgeDetector(gray_CLAHE)
            edges = edges_eq + edges_CLAHE

            img.whitening()

            # # Hough
            HoughLines = img.hough_transformation_line(edges, 0.15)

            # get mask
            mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)

            # perform GrabCut
            # imgGC = img.performGrabCut(mask, 6)

            # cut image
            imgCut = img.cutImage(hull)

            # selective search
            # s=int(scale)
            candidates = img.performSelectiveSearch(scale=2, minRectSize=100, w2h=2, useCutImage=True, sig=0.8, minSize=50)
            # print(len(candidates))

            j=0
            cPredSVM = 1
            cPredOCC_SVM = 1
            cPredMLP = 1
            cPredOCC_MLP = 1
            cPredOCC = 1

            for x, y, w, h in candidates:

                window = img.adapThres[y:y + h, x:x + w]
                winColor = img.imgCut[y:y + h, x:x + w]

                hog = classify_lib.getResizedHOGfeature(window, ppc=ppc, size=(32, 32))

                haar = classify_lib.getResizedHaarFeature(window, size=(size, size))
                haarScale = scaler.transform([haar])

                PredOCC, distOCC = classify_lib.predictOneClassSVM(clfOCC, hog)
                cPredSVM = np.min([clfSVM.predict([haar]), 1])
                cPredMLP = np.min([clfMLP.predict(haarScale), 1])

                # *********************************************************************
                # Select only values above threshold distance to decision boundary
                if distOCC < t:
                    continue

                cPredOCC = np.min([cPredOCC, PredOCC])

                predictionOCC_SVM = clfSVM.predict([haar])
                print(predictionOCC_SVM)
                print(clfSVM.decision_function([haar]))
                plt.imshow(window, cmap='gray')
                plt.title('Prediction: {}'.format(predictionOCC_SVM))
                plt.show(block=False)
                plt.pause(3)
                plt.close()
                predictionOCC_MLP = clfMLP.predict(haarScale)

                if cPredOCC == -1:
                    cPredOCC = 0

                if predictionOCC_SVM[0] == 0:
                    cPredOCC_SVM = 0
                    # continue
                if predictionOCC_MLP[0] == 0:
                    cPredOCC_MLP = 0

                # if every classifier predicts class 0, stop looking for further candidates
            # if cPredOCC == 0 & cPredOCC_SVM == 0 & cPredOCC_MLP == 0: # & cPredSVM == 0 & cPredMLP == 0:
            #         # print(cPredSVM, cPredMLP)
            #      continue

            # print(cPredSVM)
            yLabelPredictOCC.append(cPredOCC)
            yLabelPredictSVM.append(cPredSVM)
            yLabelPredictOCC_SVM.append(cPredOCC_SVM)
            yLabelPredictMLP.append(cPredMLP)
            yLabelPredictOCC_MLP.append(cPredOCC_MLP)

            s = pd.Series([yLabelTrue[i], cPredOCC, cPredSVM, cPredMLP, cPredOCC_SVM, cPredOCC_MLP],
                          index=['yTrue', 'OCC', 'SVM', 'MLP', 'OCC+SVM', 'OCC+MLP'])
            outputLabels = outputLabels.append(s, ignore_index=True)
            # print(outputLabels)
            print('OCC, SVM, MLP, OCC+SVM, OCC+MLP')
            print(cPredOCC, cPredSVM, cPredMLP, cPredOCC_SVM, cPredOCC_MLP)
        i = i+1

    # compute accuracy
    accSVM = sklearn.metrics.accuracy_score(yLabelTrue, yLabelPredictSVM)
    accOCC_SVM = sklearn.metrics.accuracy_score(yLabelTrue, yLabelPredictOCC_SVM)
    accOCC = sklearn.metrics.accuracy_score(yLabelTrue, yLabelPredictOCC)
    accMLP = sklearn.metrics.accuracy_score(yLabelTrue, yLabelPredictMLP)
    accOCC_MLP = sklearn.metrics.accuracy_score(yLabelTrue, yLabelPredictOCC_MLP)
    print(accOCC, accSVM, accMLP, accOCC_SVM, accOCC_MLP)

    # compute TPR/ FPR
    fprOCC, tprOCC= helper.computeTPR_FPR(yLabelTrue, yLabelPredictOCC)
    fprSVM, tprSVM = helper.computeTPR_FPR(yLabelTrue, yLabelPredictSVM)
    fprMLP, tprMLP = helper.computeTPR_FPR(yLabelTrue, yLabelPredictMLP)
    fprOCC_SVM, tprOCC_SVM = helper.computeTPR_FPR(yLabelTrue, yLabelPredictOCC_SVM)
    fprOCC_MLP, tprOCC_MLP = helper.computeTPR_FPR(yLabelTrue, yLabelPredictOCC_MLP)
    print('TPR:')
    print(tprOCC, tprSVM, tprMLP, tprOCC_SVM, tprOCC_MLP)
    print('FPR:')
    print(fprOCC, fprSVM, fprMLP, fprOCC_SVM, fprOCC_MLP)


    # save accuracy
    output[name + '_OCC'].iloc[0] = accOCC
    output[name+'_SVM'].iloc[0] = accSVM
    output[name + '_MLP'].iloc[0] = accMLP
    output[name + '_OCC+SVM'].iloc[0] = accOCC_SVM
    output[name + '_OCC+MLP'].iloc[0] = accOCC_MLP

    # save TPR
    output[name + '_OCC'].iloc[1] = tprOCC
    output[name + '_SVM'].iloc[1] = tprSVM
    output[name + '_MLP'].iloc[1] = tprMLP
    output[name + '_OCC+SVM'].iloc[1] = tprOCC_SVM
    output[name + '_OCC+MLP'].iloc[1] = tprOCC_MLP

    # save FPR
    output[name + '_OCC'].iloc[2] = fprOCC
    output[name + '_SVM'].iloc[2] = fprSVM
    output[name + '_MLP'].iloc[2] = fprMLP
    output[name + '_OCC+SVM'].iloc[2] = fprOCC_SVM
    output[name + '_OCC+MLP'].iloc[2] = fprOCC_MLP

output.to_csv(path_or_buf='..\output\ClassifierCompare_threshold_ROC_Test.csv', sep=',', index=False)
outputLabels.to_csv(path_or_buf='..\output\ClassifierCompare_Labels_Test.csv', sep=',', index=False)
