from timeit import default_timer as timer
import cv2
import os
from os import listdir
import matplotlib.pyplot as plt
import importlib
import scipy.misc
import selectivesearch
import matplotlib.patches as mpatches
import numpy as np
from sklearn import svm
from sklearn.metrics import roc_curve, auc
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
import sklearn
import time
from sklearn.externals import joblib
import csv

import sys
sys.path.insert(0, 'scripts')
import helper
import prepare_lib
import classify_lib
import seaborn as sns; sns.set()


ppc = (10,10)
# TRAIN OCC SVM
# path = '../images/train_icons/o_u_symbol/'
# occ_clf = classify_lib.trainOneClassSVM(TrainPath = path, OutputPath='', imgSize = (32,32), ppc = ppc, gamma = 'auto')
# joblib.dump(occ_clf, 'models/OCC_clf.pkl')
occ_clf = joblib.load('models/OCC_clf.pkl')

# # TRAIN MULTI CLASS SVM
path = '../images/train_icons/o_u_symbol/'
X_o_u_heat = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(14,14)))

path = '../images/train_icons/anti_train/'
X_anti = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(14,14)))
path_extr = '../images/train_icons/anti_extracted'
X_anti_extr = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path_extr, size=(14,14)))

X = np.concatenate((np.array(X_o_u_heat), np.array(X_anti), np.array(X_anti_extr)))
y = np.concatenate((np.zeros(len(X_o_u_heat)), np.ones(len(X_anti)), 2*np.ones(len(X_anti_extr))))

clf = svm.SVC(decision_function_shape='ovr', C=10, tol = 0.001, gamma = 0.00000001)
clf.fit(X, y)
joblib.dump(clf, 'models/MultiSVM_clf.pkl')

cwd = os.getcwd()
print(cwd)
pn = os.path.join(cwd, '../images/test_onePerspektive/10/20171118_130352.jpg')

i=0
t=-0.04

# PREPROCESSING
img = prepare_lib.prepareImage(pn)

# Preprocessing
gray_CLAHE = img.equalize_gray_Hist_CLAHE()
gray_eq = img.equalize_gray_Hist()

edges_eq = img.edgeDetector(gray_eq)
edges_CLAHE = img.edgeDetector(gray_CLAHE)
edges = edges_eq + edges_CLAHE

img.whitening()

# # Hough
HoughLines = img.hough_transformation_line(edges, 0.15)

# get mask
mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)

# perform GrabCut
# imgGC = img.performGrabCut(mask, 6)

# cut image
imgCut = img.cutImage(hull)

# selective search
# s=int(scale)
candidates = img.performSelectiveSearch(scale=2, minRectSize=100, w2h=2, useCutImage=True, sig=0.8, minSize=50)

heatmap = -1 * np.ones_like(img.adapThres, dtype=float)

j=0
cPred = 1
for x, y, w, h in candidates:

    window = img.adapThres[y:y + h, x:x + w]
    winColor = img.imgCut[y:y + h, x:x + w]
    edgesWin = img.edgeDetector(window)

    hog = classify_lib.getResizedHOGfeature(window, ppc=ppc, size=(32, 32))

    haar = classify_lib.getResizedHaarFeature(window)

    pred, dist = classify_lib.predictOneClassSVM(occ_clf, hog)
    #     print(dist)

    # if dist < t:
    #     continue

    hsv = cv2.cvtColor(winColor, cv2.COLOR_BGR2HSV)
    if np.mean(hsv[:][2]) > 200:
        print('HSV Criterion:', np.mean(hsv[:][2]))
        plt.imshow(winColor)
        plt.show(block=False)
        plt.pause(3)
        plt.close()
        continue

    prediction = clf.predict([haar])
    distCLF = clf.decision_function([haar])

    # print('occ-distance: ', dist)
    # print('prediction: ', prediction, distCLF[0][0])

    for xw in range(x,x+w):
        for yw in range(y,y+h):
            heatmap[yw][xw] = np.max([heatmap[yw][xw], distCLF[0][0]])


    if prediction[0] == 0:
        cPred = 0
        continue

ax = sns.heatmap(heatmap, center=0, square=True, cmap='bwr', cbar_kws={"orientation": "horizontal", 'label': 'distance'})
ax.axes.get_xaxis().set_visible(False)
ax.axes.get_yaxis().set_visible(False)
ax.imshow(edgesWin)
plt.tight_layout()
plt.show()