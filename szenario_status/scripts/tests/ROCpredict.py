from __future__ import (
    division,
    print_function,
)

import skimage.data
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import scipy.signal
import selectivesearch
import cv2
import imutils
import os
from os import listdir
import glob
from sklearn.externals import joblib
from timeit import default_timer as timer
import xlsxwriter

import helper
from helper import *
import prepare_lib
from prepare_lib import *
import classify_lib
from classify_lib import *



clf = joblib.load('models/Classifier.pkl')

pn10 = '../images/test/10'
pn11 = '../images/test/11'
pn01 = '../images/test/01'
pn00 = '../images/test/00'

row = 2
files = listdir(os.path.join(pn))


for pic in files:
    p = os.path.join(pn+ "\\" +pic)
    print(p)
    img = prepare_lib.prepareImage(p)

    ###### Preprocessing

    edges = img.edgeDetector(img.adapThres)

    # # Hough
    HoughLines = img.hough_transformation_line(edges, 0.22)

    # get mask
    mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)

    # cut image
    img.cutImage(hull)

    # get window candidates
    candidates = img.performSelectiveSearch(450, minRectSize=2000, w2h=1.4, useCutImage=True)
    candidates = helper.reduceRect(candidates)

    # perform classification on image segments

    for x, y, w, h in candidates:
        # print(x, y, w, h)
        # start = timer()
        section = img.adapThres[y:y+h, x:x+w]
        sectionColor = img.img[y:y + h, x:x + w]
        M, N = section.shape

        winx = winy = np.min([np.uint8(np.floor(N/10)), np.uint8(np.floor(M / 10))])
        # print(winx, winy)

        stepsize = 4
        heatmaps = classifyAndCreateHeatmap(section, clf, stepSize=stepsize, winSize=(winx, winy), showSlidingWindow=False)

        bestlevel, gmax = getBestLevel(heatmaps, 3)
