from timeit import default_timer as timer
import cv2
import os
from os import listdir
import matplotlib.pyplot as plt
import importlib
import scipy.misc
import selectivesearch
import matplotlib.patches as mpatches
import numpy as np
from sklearn import svm
from sklearn.metrics import roc_curve, auc
import sklearn
import time
from sklearn.externals import joblib

import sys
sys.path.insert(0, 'scripts')
import helper
import prepare_lib
import classify_lib
from numba import jit


@jit
def perImageClassification(basepathList, clf, occ_clf):
    yLabelPredict = []
    for basepath in basepathList:
        print ('*****************************')
        print(basepath)
        print(os.listdir(basepath))
        print ('*****************************')
        for file in os.listdir(basepath):

            pn = os.path.join(basepath, file)
            print(pn)

            # PREPROCESSING
            img = prepare_lib.prepareImage(pn)

            # Preprocessing
            gray_CLAHE = img.equalize_gray_Hist_CLAHE()
            gray_eq = img.equalize_gray_Hist()

            edges_eq = img.edgeDetector(gray_eq)
            edges_CLAHE = img.edgeDetector(gray_CLAHE)
            edges = edges_eq + edges_CLAHE

            img.whitening()

            # # Hough
            HoughLines = img.hough_transformation_line(edges, 0.15)

            # get mask
            mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)

            # perform GrabCut
            # imgGC = img.performGrabCut(mask, 6)

            # cut image
            imgCut = img.cutImage(hull)

            # selective search
            # s=int(scale)
            candidates = img.performSelectiveSearch(scale=2, minRectSize=100, w2h=2, useCutImage=True, sig=0.8,
                                                    minSize=50)

            j = 0
            cPred = 1

            for x, y, w, h in candidates:

                window = img.adapThres[y:y + h, x:x + w]
                winColor = img.imgCut[y:y + h, x:x + w]

                hog = classify_lib.getResizedHOGfeature(window, ppc=ppc, size=(32, 32))

                haar = classify_lib.getResizedHaarFeature(window)

                pred, dist = classify_lib.predictOneClassSVM(occ_clf, hog)
                #     print(dist)

                if dist < t:  # -0.02:  # pred == 1:#dist > -0.05:
                    continue

                # TODO: add threshold on value (HSV) for classification !!!!!
                hsv = cv2.cvtColor(winColor, cv2.COLOR_BGR2HSV)
                if np.mean(hsv[:][2]) > 200:
                    print('HSV Criterion:', np.mean(hsv[:][2]))
                    plt.imshow(winColor)
                    plt.show(block=False)
                    plt.pause(3)
                    plt.close()
                    continue

                # print('************', haar.shape)
                # print(haar)
                # print('************')
                # print('OCC Distance:', dist)
                prediction = clf.predict([haar])
                distCLF = clf.decision_function([haar])

                # print('occ-distance: ', dist)
                # print('prediction: ', prediction, distCLF)
                if prediction[0] == 0:
                    cPred = 0
                    continue

            yLabelPredict.append(cPred)
            print(cPred)
        print(yLabelPredict)
        return yLabelPredict



ppc = (10,10)
# TRAIN OCC SVM
path = 'C:/Users/alici/Uni/Bayer_Praktikum/szenario_status/images/train_icons/o_u_symbol/'
occ_clf = classify_lib.trainOneClassSVM(TrainPath = path, OutputPath='', imgSize = (32,32), ppc = ppc, gamma = 'auto')

# # TRAIN MULTI CLASS SVM
path = '../images/train_icons/o_u_symbol/'
X_o_u_heat = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(14,14)))

path = '../images/train_icons/anti_train/'
X_anti = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(14,14)))
path_extr = '../images/train_icons/anti_extracted'
X_anti_extr = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path_extr, size=(14,14)))

X = np.concatenate((np.array(X_o_u_heat), np.array(X_anti), np.array(X_anti_extr)))
y = np.concatenate((np.zeros(len(X_o_u_heat)), np.ones(len(X_anti)), 2*np.ones(len(X_anti_extr))))

clf = svm.SVC(decision_function_shape='ovr', C=10, tol = 0.001, gamma = 0.00000001)
clf.fit(X, y)

# basepath = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\test_onePerspektive\\11'
basepath00 = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\test_onePerspektive\\short\\00'
basepath01 = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\test_onePerspektive\\short\\01'
basepath10 = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\test_onePerspektive\\short\\10'
basepath11 = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\test_onePerspektive\\short\\11'
basepathList = [basepath11, basepath10, basepath01, basepath00] #[basepath00, basepath01, basepath10, basepath11]
outPath = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\train_icons\\anti'

# print(len(os.listdir(basepath11)), len(os.listdir(basepath10)), len(os.listdir(basepath01)), len(os.listdir(basepath00)))

# yLabelTrue = np.concatenate([np.zeros(len(os.listdir(basepath10))), np.zeros(len(os.listdir(basepath11)))])
yLabelTrue = np.concatenate([np.zeros(len(os.listdir(basepath11))), np.zeros(len(os.listdir(basepath10))),
                             np.ones(len(os.listdir(basepath01))), np.ones(len(os.listdir(basepath00)))])
print(yLabelTrue)

i=0
fpr = dict()
tpr = dict()
roc_auc = dict()
for t in [ -0.05, -0.04, -0.03, -0.02]:
    print('**********************************   OCC Threshold  **********************************')
    # size = (int(s),int(s))
    print(t)

    yLabelPredict = perImageClassification(basepathList, clf, occ_clf)


    fpr[i], tpr[i], _ = roc_curve(yLabelTrue[:], yLabelPredict[:])
    roc_auc[i] = auc(fpr[i], tpr[i])
    i=i+1

    acc = sklearn.metrics.accuracy_score(yLabelTrue, yLabelPredict)
    print(acc)
    try:
        print('TPR, FPR:' , tpr[i], fpr[i])
        print('AUC:', roc_auc[i])
    except:
        continue


print('Zusammenfassung:')
print('TPR, FPR:' , tpr, fpr)
print('AUC:', roc_auc)

plt.figure()
lw = 2
plt.plot(fpr, tpr, color='darkorange',
         lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[2])
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")
plt.show()


