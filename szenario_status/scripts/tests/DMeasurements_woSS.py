from __future__ import (
    division,
    print_function,
)

import skimage.data
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import scipy.signal
import selectivesearch
import cv2
import imutils
import os
from os import listdir
import glob
from sklearn.externals import joblib
from timeit import default_timer as timer
import xlsxwriter

import helper
from helper import *
import prepare_lib
from prepare_lib import *
import classify_lib
from classify_lib import *



clf = joblib.load('models/Classifier_32_HOG44.pkl')

pn = '../images/train/10' #test_onePerspektive\11

row = 2
files = listdir(os.path.join(pn))
# print(files)

workbook = xlsxwriter.Workbook('../output/DistanceMeasure_32_HOG44.xlsx')
worksheet = workbook.add_worksheet()

# Widen the first column to make the text clearer.
worksheet.set_column('A:A', 20)
# write header
worksheet.write('A1', 'path')
worksheet.write('B1', 'candidate 1')
worksheet.write('C1', 'candidate 2')
worksheet.write('D1', 'candidate 3')
worksheet.write('E1', 'candidate 4')


for pic in files:
    p = os.path.join(pn+ "\\" +pic)
    print(p)
    img = prepare_lib.prepareImage(p)

    ###### Preprocessing

    gray_eq = img.equalize_gray_Hist()
    gray_CLAHE = img.equalize_gray_Hist_CLAHE()

    edges_eq = img.edgeDetector(gray_eq)
    edges_CLAHE = img.edgeDetector(gray_CLAHE)
    # edges = edges_eq + edges_CLAHE
    edges = img.edgeDetector(img.adapThres)

    # # Hough
    HoughLines = img.hough_transformation_line(edges, 0.22)

    # get mask
    mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)

    # cut image
    img.cutImage(hull)

    # # get window candidates
    # candidates = img.performSelectiveSearch(450, minRectSize=2000, w2h=1.4, useCutImage=True)
    # candidates = helper.reduceRect(candidates)

    worksheet.write(row, 0, pic)
    # perform classification on image segments
    c = 1
    # for x, y, w, h in candidates:
        # print(x, y, w, h)
        # start = timer()
    section = img.adapThres
        # sectionColor = img.img[y:y + h, x:x + w]
        # M, N = section.shape

    # winx = winy = np.min([np.uint8(np.floor(N/10)), np.uint8(np.floor(M / 10))])
    # print(winx, winy)

    stepsize = 4
    heatmaps, yclass = classifyAndCreateHeatmap(section, clf, stepSize=stepsize, winSize=(32, 32), showSlidingWindow=False)

    bestlevel, gmax = getBestLevel(heatmaps, 3)
    # print(bestlevel, gmax[bestlevel])


    try:
        top3 = helper.getTopNvalues(heatmaps, bestlevel, 3)
        h0 = heatmaps[bestlevel]
        h0M, h0N = h0.shape
        cmap, norm = helper.defineCenteredCmap('bwr', -0.2, gmax[bestlevel])

        fig, ax = plt.subplots(ncols=2, nrows=1)
        section = cv2.resize(section, (h0N, h0M))
        ax[0].imshow(section, cmap='gray')

        cax = ax[1].imshow(h0, norm=norm, cmap=cmap, vmin=-0.2, vmax=np.max(top3))
        cbar = fig.colorbar(cax, orientation='vertical')

        for val in top3:
            ind = np.argwhere(h0 == val)
            (y, x, h, w) = cv2.boundingRect(ind)

            rect = mpatches.Rectangle((x, y), w, h, fill=False, edgecolor='green', linewidth=2)
            ax[0].add_patch(rect)
            ax[1].add_patch(rect)
    except ValueError:
        print('Bild kann nicht angezeigt werden')

    # duration = timer() - start
    # print(duration)


    worksheet.write(row, c, gmax[bestlevel])
    c = c+1
    plt.show()
    row += 1

workbook.close()
