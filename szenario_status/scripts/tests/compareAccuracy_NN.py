from timeit import default_timer as timer
import cv2
import os
from os import listdir
import matplotlib.pyplot as plt
import importlib
import scipy.misc
import selectivesearch
import matplotlib.patches as mpatches
import numpy as np
from sklearn import svm
from sklearn.metrics import roc_curve, auc
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
import sklearn
import time
from sklearn.externals import joblib
import csv
import pandas as pd

import sys
sys.path.insert(0, 'scripts')
import helper
import prepare_lib
import classify_lib

ppc = (10,10)
# TRAIN OCC SVM
# path = '../images/train_icons/o_u_symbol/'
# occ_clf = classify_lib.trainOneClassSVM(TrainPath = path, OutputPath='', imgSize = (32,32), ppc = ppc, gamma = 'auto')
# joblib.dump(occ_clf, 'models/OCC_clf.pkl')
occ_clf = joblib.load('models/OCC_clf.pkl')

# # TRAIN MULTI CLASS SVM
path = '../images/train_icons/o_u_symbol/'
X_o_u_heat = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(14,14)))

path = '../images/train_icons/anti_train/'
X_anti = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(14,14)))
path_extr = '../images/train_icons/anti_extracted'
X_anti_extr = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path_extr, size=(14,14)))

X = np.concatenate((np.array(X_o_u_heat), np.array(X_anti), np.array(X_anti_extr)))
Y = np.concatenate((np.zeros(len(X_o_u_heat)), np.ones(len(X_anti)), 2*np.ones(len(X_anti_extr))))

# clf = svm.SVC(decision_function_shape='ovr', C=10, tol = 0.001, gamma = 0.00000001)

scaler = StandardScaler()
# Don't cheat - fit only on training data
scaler.fit(X)
X = scaler.transform(X)
# clf = MLPClassifier(solver='lbfgs', alpha=10,
#                     hidden_layer_sizes=(20,), random_state=1, warm_start=True)


# clf.fit(X, y)
# joblib.dump(clf, 'models/MultiSVM_clf.pkl')

cwd = os.getcwd()
print(cwd)
basepath00 = os.path.join(cwd, '..\\images\\test_onePerspektive\\00')
basepath01 = os.path.join(cwd, '..\\images\\test_onePerspektive\\01')
basepath10 = os.path.join(cwd, '..\\images\\test_onePerspektive\\10')
basepath11 = os.path.join(cwd, '..\\images\\test_onePerspektive\\11')
basepathList = [basepath11, basepath10, basepath01, basepath00]
outPath = os.path.join(cwd, '..\\images\\train_icons\\anti')

yLabelTrue = np.concatenate([np.zeros(len(os.listdir(basepath11))), np.zeros(len(os.listdir(basepath10))),
                             np.ones(len(os.listdir(basepath01))), np.ones(len(os.listdir(basepath00)))])
# print(yLabelTrue)

output = pd.DataFrame([[0 , 0 , 0]], columns=['LBFGS_3_27', 'LBFGS_3_28', 'LBFGS_3_29'])

i=0
fpr = dict()
tpr = dict()
roc_auc = dict()
for layer, hls, name in zip([3, 3, 3],[27, 28, 29],
                            ['LBFGS_3_27', 'LBFGS_3_28', 'LBFGS_3_29']):
    t=-0.04
    if layer == 2:
        clf = MLPClassifier(solver='lbfgs', alpha=10, hidden_layer_sizes=(hls, hls), random_state=1, warm_start=True)
    else:
        clf = MLPClassifier(solver='lbfgs', alpha=10, hidden_layer_sizes=(hls, hls, hls), random_state=1, warm_start=True)
    clf.fit(X, Y)

    print('**********************************   name **********************************')
    print(name)

    yLabelPredict = []
    for basepath in basepathList:
        print ('*****************************')
        print(basepath)
        print(os.listdir(basepath))
        print ('*****************************')
        # filelist = []
        for file in os.listdir(basepath):

            pn = os.path.join(basepath, file)
            # print('PN:', pn)
            # filelist.append(pn)
            # print(filelist)

            # PREPROCESSING
            img = prepare_lib.prepareImage(pn)

            # Preprocessing
            gray_CLAHE = img.equalize_gray_Hist_CLAHE()
            gray_eq = img.equalize_gray_Hist()

            edges_eq = img.edgeDetector(gray_eq)
            edges_CLAHE = img.edgeDetector(gray_CLAHE)
            edges = edges_eq + edges_CLAHE

            img.whitening()

            # # Hough
            HoughLines = img.hough_transformation_line(edges, 0.15)

            # get mask
            mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)

            # cut image
            imgCut = img.cutImage(hull)

            # selective search
            # s=int(scale)
            candidates = img.performSelectiveSearch(scale=2, minRectSize=100, w2h=2, useCutImage=True, sig=0.8, minSize=50)

            j=0
            cPred = 1

            for x, y, w, h in candidates:

                window = img.adapThres[y:y + h, x:x + w]
                winColor = img.imgCut[y:y + h, x:x + w]

                hog = classify_lib.getResizedHOGfeature(window, ppc=ppc, size=(32, 32))

                haar = classify_lib.getResizedHaarFeature(window)
                # apply same transformation to test data
                haar = scaler.transform([haar])

                pred, dist = classify_lib.predictOneClassSVM(occ_clf, hog)
                #     print(dist)

                if dist < t:    #-0.02:  # pred == 1:#dist > -0.05:
                    continue

                # TODO: add threshold on value (HSV) for classification !!!!!
                hsv = cv2.cvtColor(winColor, cv2.COLOR_BGR2HSV)
                if np.mean(hsv[:][2]) > 200:
                    print('HSV Criterion:', np.mean(hsv[:][2]))
                    plt.imshow(winColor)
                    plt.show(block=False)
                    plt.pause(3)
                    plt.close()
                    continue

                # print('************', haar.shape)
                # print(haar)
                # print('************')
                print('OCC Distance:', dist)
                prediction = clf.predict(haar)
                # prediction = clf.predict([haar])
                # distCLF = clf.decision_function([haar])

                # print('occ-distance: ', dist)
                # print('prediction: ', prediction, distCLF)
                if prediction[0] == 0:
                    cPred = 0
                    continue

                # if prediction[0] == 0:
                #     # fileOut = file.split('.')[0]
                #     # name = outPath+'\extracted_{0}_{1}.png'.format(fileOut, j)
                #     # # print('Write to Output File: ', name)
                #     # cv2.imwrite(name,winColor)
                #     # j=j+1
                #
                #     # cv2.namedWindow('Prediction: {}'.format(prediction))
                #     # cv2.imshow('image', winColor)
                #     plt.imshow(window, cmap='gray')
                #     plt.title('Prediction: {}'.format(prediction))
                #     plt.show(block=False)
                #     plt.pause(3)
                #     plt.close()
                #     # cv2.waitKey(0)
                #     # cv2.destroyAllWindows()
            yLabelPredict.append(cPred)
            print(cPred)
        # yLabelPredict.append(yLabelPredictTemp)
        print(yLabelPredict)

    # fpr[i], tpr[i], _ = roc_curve(yLabelTrue[:], yLabelPredict[:])
    # roc_auc[i] = auc(fpr[i], tpr[i])
    i=i+1

    acc = sklearn.metrics.accuracy_score(yLabelTrue, yLabelPredict)
    print(acc)
    # temp={}
    # temp = {'LBFGS_10', 'LBFGS_20', 'SGD_10', 'SGD_20', 'ADAM_10', 'ADAM_20']}
    output[name].iloc[0]=acc

    # try:
    #     print('TPR, FPR:' , tpr[i], fpr[i])
    #     print('AUC:', roc_auc[i])
    # except:
    #     continue

output.to_csv(path_or_buf='..\output\MLPcompareLayerSize3_26to29.csv', sep=',', index=False)
# print('***** write CSV file *******')
# with open('../Test_Treshold.csv', 'wb') as f:
#     writer = csv.writer(f)
#     writer.writerows((filelist, yLabelTrue, yLabelPredict))


# print('Zusammenfassung:')
# print('TPR, FPR:' , tpr, fpr)
# print('AUC:', roc_auc)
#
# plt.figure()
# lw = 2
# plt.plot(fpr[0], tpr[0], color='darkorange',
#          lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[2])
# plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
# plt.xlim([0.0, 1.0])
# plt.ylim([0.0, 1.05])
# plt.xlabel('False Positive Rate')
# plt.ylabel('True Positive Rate')
# plt.title('Receiver operating characteristic example')
# plt.legend(loc="lower right")
# plt.show()
yLabelPredict = []
