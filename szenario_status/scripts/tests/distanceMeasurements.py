from __future__ import (
    division,
    print_function,
)

import skimage.data
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import scipy.signal
import selectivesearch
import cv2
import imutils
import os
from os import listdir
import glob
from sklearn.externals import joblib
from timeit import default_timer as timer

import pandas as pd

import helper
from helper import *
import prepare_lib
from prepare_lib import *
import classify_lib
from classify_lib import *


clf = joblib.load('models/Classifier.pkl')

pn = '../images/train/10'
files = listdir(os.path.join(pn))

columns = ['path', 'cd_1','cd_2','cd_3','cd_4',
    'x_1', 'y_1', 'w_1', 'h_1', 'x_2', 'y_2', 'w_2', 'h_2',
    'x_3', 'y_3', 'w_3', 'h_3', 'x_4', 'y_4', 'w_4', 'h_4',]
labels = pd.DataFrame(columns=columns)


for pic in files:
    p = os.path.join(pn+ "\\" +pic)
    print(p)
    img = prepare_lib.prepareImage(p)

    ###### Preprocessing

    # gray_eq = img.equalize_gray_Hist()
    # gray_CLAHE = img.equalize_gray_Hist_CLAHE()
    #
    # edges_eq = img.edgeDetector(gray_eq)
    # edges_CLAHE = img.edgeDetector(gray_CLAHE)
    # edges = edges_eq + edges_CLAHE
    edges = img.edgeDetector(img.adapThres)

    # # Hough
    # HoughLines = img.hough_transformation_line(edges, 0.22)
    #
    # # get mask
    # mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)
    #
    # # cut image
    # img.cutImage(hull)
    #
    # # get window candidates
    # candidates = img.performSelectiveSearch(450, minRectSize=2000, w2h=1.4, useCutImage=True)
    # candidates = helper.reduceRect(candidates)
    #
    # worksheet.write(row, 0, pic)
    # # perform classification on image segments
    # c = 1
    # for x, y, w, h in candidates:
    #     # print(x, y, w, h)
    #     start = timer()
    #     section = img.adapThres[y:y+h, x:x+w]
    #     M, N = section.shape
    #
    #     winx = winy = np.min([np.uint8(np.floor(N/12)), np.uint8(np.floor(M / 12))])
    #     # print(winx, winy)
    #
    #     stepsize = 4
    #     heatmaps = classifyAndCreateHeatmap(section, clf, stepSize=stepsize, winSize=(winx, winy), showSlidingWindow=False)
    #
    #     bestlevel, gmax = getBestLevel(heatmaps, 3)
    #     print(bestlevel, gmax[bestlevel])
    #
    #     duration = timer() - start
        # print(duration)


        # worksheet.write(row, c, gmax[bestlevel])
        # c = c+1
    # row += 1

# workbook.close()
