from timeit import default_timer as timer
import cv2
import os
from os import listdir
import matplotlib.pyplot as plt
import importlib
import scipy.misc
import selectivesearch
import matplotlib.patches as mpatches
import numpy as np
from sklearn import svm
from sklearn.metrics import roc_curve, auc
import sklearn
import time
from sklearn.externals import joblib

import sys
sys.path.insert(0, 'scripts')
import helper
import prepare_lib
import classify_lib

ppc = (10,10)
# TRAIN OCC SVM
path = 'C:/Users/alici/Uni/Bayer_Praktikum/szenario_status/images/train_icons/o_u_symbol/'
occ_clf = classify_lib.trainOneClassSVM(TrainPath = path, OutputPath='', imgSize = (32,32), ppc = ppc, gamma = 'auto')

# # TRAIN MULTI CLASS SVM
path = '../images/train_icons/o_u_symbol/'
X_o_u_heat = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(14,14)))

path = '../images/train_icons/anti_train/'
X_anti = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path, size=(14,14)))
path_extr = '../images/train_icons/anti_extracted'
X_anti_extr = np.array(classify_lib.extractTrainFeaturesMultiClass(path=path_extr, size=(14,14)))

X = np.concatenate((np.array(X_o_u_heat), np.array(X_anti), np.array(X_anti_extr)))
y = np.concatenate((np.zeros(len(X_o_u_heat)), np.ones(len(X_anti)), 2*np.ones(len(X_anti_extr))))

clf = svm.SVC(decision_function_shape='ovr', C=10, tol = 0.001, gamma = 0.00000001)
clf.fit(X, y)

basepathList = ['C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\train\\neu']
outPath = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\train_icons\\'

for basepath in basepathList:
    print ('*****************************')
    print(basepath)
    print(os.listdir(basepath))
    print ('*****************************')
    for file in os.listdir(basepath):

        pn = os.path.join(basepath, file)
        print(pn)

        # PREPROCESSING
        img = prepare_lib.prepareImage(pn)

        # Preprocessing
        gray_CLAHE = img.equalize_gray_Hist_CLAHE()
        gray_eq = img.equalize_gray_Hist()

        edges_eq = img.edgeDetector(gray_eq)
        edges_CLAHE = img.edgeDetector(gray_CLAHE)
        edges = edges_eq + edges_CLAHE

        img.whitening()

        # # Hough
        HoughLines = img.hough_transformation_line(edges, 0.15)

        # get mask
        mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)

        # perform GrabCut
        # imgGC = img.performGrabCut(mask, 6)

        # cut image
        imgCut = img.cutImage(hull)

        # selective search
        # s=int(scale)
        candidates = img.performSelectiveSearch(scale=1, minRectSize=100, w2h=2, useCutImage=True, sig=0.8, minSize=50)

        j=0
        cPred = 1

        for x, y, w, h in candidates:

            window = img.adapThres[y:y + h, x:x + w]
            winColor = img.imgCut[y:y + h, x:x + w]

            hog = classify_lib.getResizedHOGfeature(window, ppc=ppc, size=(32, 32))

            haar = classify_lib.getResizedHaarFeature(window)

            pred, dist = classify_lib.predictOneClassSVM(occ_clf, hog)
            #     print(dist)

            if dist < -0.13:  # pred == 1:#dist > -0.05:
                continue

            # TODO: add threshold on value (HSV) for classification !!!!!
            hsv = cv2.cvtColor(winColor, cv2.COLOR_BGR2HSV)
            if np.mean(hsv[:][2]) > 180:
                print('HSV Criterion:', np.mean(hsv[:][2]))
                plt.imshow(winColor)
                plt.show(block=False)
                plt.pause(3)
                plt.close()
                continue

            # print('************', haar.shape)
            # print(haar)
            # print('************')
            print('OCC Distance:', dist)
            prediction = clf.predict([haar])
            distCLF = clf.decision_function([haar])

            # print('occ-distance: ', dist)
            # print('prediction: ', prediction, distCLF)
            if prediction[0] == 0:
                cPred = 0

            fileOut = file.split('.')[0]
            name = outPath+'\extracted_{0}_{1}.png'.format(fileOut, j)
            # print('Write to Output File: ', name)
            cv2.imwrite(name,winColor)
            j=j+1

            # cv2.namedWindow('Prediction: {}'.format(prediction))
            # cv2.imshow('image', winColor)
            plt.imshow(window, cmap='gray')
            plt.title('Prediction: {}'.format(prediction))
            plt.show(block=False)
            plt.pause(3)
            plt.close()