from timeit import default_timer as timer
import cv2
import os
from os import listdir
import matplotlib.pyplot as plt
import importlib
import scipy.misc
import selectivesearch
import matplotlib.patches as mpatches
import numpy as np
from sklearn import svm
import sklearn

import sys
sys.path.insert(0, 'scripts')
import helper
import prepare_lib
import classify_lib

# basepath = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\test_onePerspektive\\11'
basepath00 = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\test\\00'
basepath01 = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\test\\01'
basepath10 = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\test\\10'
basepath11 = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\test\\11'
# basepathList = [basepath00, basepath01, basepath10, basepath11] #[basepath00, basepath01, basepath10, basepath11]
basepathList = ['C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\train\\neu']
outPath = 'C:\\Users\\alici\\Uni\\Bayer_Praktikum\\szenario_status\\images\\train_icons\\'



for basepath in basepathList:
    print ('*****************************')
    print(basepath)
    print(os.listdir(basepath))
    print ('*****************************')
    for file in os.listdir(basepath):

        pn = os.path.join(basepath, file)
        print(pn)

        # PREPROCESSING
        img = prepare_lib.prepareImage(pn)

        # Preprocessing
        gray_eq = img.equalize_gray_Hist()
        gray_CLAHE = img.equalize_gray_Hist_CLAHE()

        edges_eq = img.edgeDetector(gray_eq)
        edges_CLAHE = img.edgeDetector(gray_CLAHE)
        edges = edges_eq + edges_CLAHE

        # # Hough
        HoughLines = img.hough_transformation_line(edges, 0.15)

        # get mask
        mask, hull = helper.extractMask(img.img.shape[0], img.img.shape[1], HoughLines)

        # cut image
        imgCut = img.cutImage(hull)

        # selective search
        candidates = img.performSelectiveSearch(scale=1, minRectSize=100, w2h=2, useCutImage=True, sig=0.8, minSize=50)
        print('Anzahl Candidates:', len(candidates))

        # draw rectangles on the original image
        fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(6, 6))
        ax.imshow(img.imgCut)
        for x, y, w, h in candidates:
            rect = mpatches.Rectangle((x, y), w, h, fill=False, edgecolor='red', linewidth=2)
            ax.add_patch(rect)
        plt.show()