
# coding: utf-8

import os
import pandas as pd

rootdir = os.getcwd()
rootdir = rootdir + '\..\\'

labels = pd.DataFrame(columns=['path', 'function', 'temperature', 'merged'])

for subdir, dirs, files in os.walk(rootdir):
    bit = subdir.split('\\')[-1]

    if(len(bit) == 2):
        for file in files:
            merged = None
            if(bit[0] == '1' and bit[1] == '1'):
                merged = '1'
            else:
                merged = '0'

            labels = labels.append({'path': file, 'function': bit[0], 'temperature': bit[1], 'merged': merged}, ignore_index=True)

labels.to_csv(path_or_buf='../output/labels.csv', sep=',', index=False)

print('saving label-images successfull')
